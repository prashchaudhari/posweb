﻿app.controller("CustomerController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.CustomerList = function () {

        $http({
            url: '../api/Customer/GetCustomerList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.Comapany = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Comapany.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Comapany, params.orderBy()) : $scope.Comapany;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.CustomerList();

    $scope.GetCityList = function () {

        $http({
            url: '../api/City/GetCityList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.CityList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetCountryList = function () {

        $http({
            url: '../api/Country/GetCountryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.CountryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetCustTypeList = function () {

        $http({
            url: '../api/CustType/GetCustTypeList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.CustTypeList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }

    $scope.AddCustomer = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
        $scope.GetCityList();
        $scope.GetCountryList();
        $scope.GetCustTypeList();
    }

    $scope.CancelCustomer = function () {
        $scope.panel = true;
    }

    $scope.SubmitCustomer = function () {
        if ($scope.Validation() != false) {

            var master = {
                "ShortName": $scope.ShortName,
                "LongName": $scope.LongName,
                "Address": $scope.Address,
                "Address1": $scope.Address1,

                "Area": $scope.Area,
                "Street": $scope.Street,
                "District": $scope.District,
                "CityID": $scope.CityID,
                
                "CountryID": $scope.CountryID,
                "Region": $scope.Region,
                "Phone": $scope.Phone,
                "Phone1": $scope.Phone1,

                "Fax": $scope.Fax,
                "website": $scope.website,
                "Mobile": $scope.Mobile,
                "email": $scope.Email,

                "CreditLimit": $scope.CreditLimit,               
                "CreditDays": $scope.CreditDays,
                "Active": $scope.Active,
                "PriceList": $scope.PriceList,

                "Discount": $scope.Discount,
                "CustTypeID": $scope.CustTypeID,
                 "Created_by": userid
            };

            $http({
                url: '../api/Customer/SaveCustomer',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CustomerList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditCustomer = function (rowdata) {
        $scope.GetCityList();
        $scope.GetCountryList();
        $scope.GetCustTypeList();
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.CustID,
            $scope.ShortName = rowdata.ShortName,
            $scope.LongName = rowdata.LongName,
            $scope.Area = rowdata.Area,
            $scope.Street = rowdata.Street,

            $scope.Address = rowdata.Address,
            $scope.Address1 = rowdata.Address1,
            $scope.Phone1 = rowdata.Phone1,
            $scope.Phone = rowdata.Phone,

            $scope.District = rowdata.District,
            $scope.CityID = rowdata.CityID,
            $scope.CountryID = rowdata.CountryID,
            $scope.Region = rowdata.Region,

            $scope.Fax = rowdata.Fax,
            $scope.CreditLimit = rowdata.CreditLimit,
            $scope.Mobile = rowdata.Mobile,
            $scope.Email = rowdata.Email,

            $scope.website = rowdata.website,
            $scope.CreditDays = rowdata.CreditDays,
            $scope.Active = rowdata.Active,
            $scope.PriceList = rowdata.PriceList,

            $scope.Discount = rowdata.Discount,            
            $scope.CustTypeID = rowdata.CustTypeID
    }

    $scope.UpdateCustomer = function () {
        if ($scope.Validation() != false) {
            var master = {
                "CustID": bid,
                "ShortName": $scope.ShortName,
                "LongName": $scope.LongName,
                "Address": $scope.Address,
                "Address1": $scope.Address1,

                "Area": $scope.Area,
                "Street": $scope.Street,
                "District": $scope.District,
                "CityID": $scope.CityID,

                "CountryID": $scope.CountryID,
                "Region": $scope.Region,
                "Phone": $scope.Phone,
                "Phone1": $scope.Phone1,

                "Fax": $scope.Fax,
                "website": $scope.website,
                "Mobile": $scope.Mobile,
                "email": $scope.Email,

                "CreditLimit": $scope.CreditLimit,
                "CreditDays": $scope.CreditDays,
                "Active": $scope.Active,
                "PriceList": $scope.PriceList,

                "Discount": $scope.Discount,
                "CustTypeID": $scope.CustTypeID,
                "Created_by": userid
            };

            $http({
                url: '../api/Customer/SaveCustomer',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (data) {

                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CustomerList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteCustomer = function (id) {

        var master = {
            "CustID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Customer/SaveCustomer',
            data: master,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (branch) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.CustomerList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.ShortName == "" || $scope.ShortName == undefined || $scope.ShortName == null) {
            $scope.errormessage = "Enter ShortName";
            return false;
        }
        if ($scope.LongName == "" || $scope.LongName == undefined || $scope.LongName == null) {
            $scope.errormessage = "Enter LongName"; return false;
        }
        if ($scope.District == "" || $scope.District == undefined || $scope.District == null) {
            $scope.errormessage = "Enter District"; return false;
        }
        if ($scope.CityID == "" || $scope.CityID == undefined || $scope.CityID == null) {
            $scope.errormessage = "Select City Name"; return false;
        }

        if ($scope.Area == "" || $scope.Area == undefined || $scope.Area == null) {
            $scope.errormessage = "Enter Area"; return false;
        }
        if ($scope.CountryID == "" || $scope.CountryID == undefined || $scope.CountryID == null) {
            $scope.errormessage = "Select Country Name"; return false;
        }
        if ($scope.Address == "" || $scope.Address == undefined || $scope.Address == null) {
            $scope.errormessage = "Enter Address";
            return false;
        }
        if ($scope.Address1 == "" || $scope.Address1 == undefined || $scope.Address1 == null) {
            $scope.errormessage = "Enter Another Addresss"; return false;
        }

        if ($scope.Street == "" || $scope.Street == undefined || $scope.Street == null) {
            $scope.errormessage = "Enter Street"; return false;
        }
        if ($scope.Region == "" || $scope.Region == undefined || $scope.Region == null) {
            $scope.errormessage = "Enter Region"; return false;
        }
        if ($scope.Phone1 == "" || $scope.Phone1 == undefined || $scope.Phone1 == null) {
            $scope.errormessage = "Enter Antoher Phone"; return false;
        }
        if ($scope.Phone == "" || $scope.Phone == undefined || $scope.Phone == null) {
            $scope.errormessage = "Enter Phone"; return false;
        }

        if ($scope.Fax == "" || $scope.Fax == undefined || $scope.Fax == null) {
            $scope.errormessage = "Enter Fax";
            return false;
        }
        if ($scope.Mobile == "" || $scope.Mobile == undefined || $scope.Mobile == null) {
            $scope.errormessage = "Enter Mobile"; return false;
        }
        if ($scope.Email == "" || $scope.Email == undefined || $scope.Email == null) {
            $scope.errormessage = "Enter Email"; return false;
        }
        if ($scope.website == "" || $scope.website == undefined || $scope.website == null) {
            $scope.errormessage = "Enter website"; return false;
        }

        if ($scope.CreditLimit == "" || $scope.CreditLimit == undefined || $scope.CreditLimit == null) {
            $scope.errormessage = "Enter CreditLimit"; return false;
        }
        if ($scope.CreditDays == "" || $scope.CreditDays == undefined || $scope.CreditDays == null) {
            $scope.errormessage = "Enter CreditDays"; return false;
        }
        //if ($scope.Active == "" || $scope.Active == undefined || $scope.Active == null) {
        //    $scope.errormessage = "Checked"; return false;
        //}
        if ($scope.PriceList == "" || $scope.PriceList == undefined || $scope.PriceList == null) {
            $scope.errormessage = "Enter PriceList"; return false;
        }
        if ($scope.Discount == "" || $scope.Discount == undefined || $scope.Discount == null) {
            $scope.errormessage = "Enter Discount"; return false;
        }        
        if ($scope.CustTypeID == "" || $scope.CustTypeID == undefined || $scope.CustTypeID == null) {
            $scope.errormessage = "Select Customer Type"; return false;
        }
        return true;
    }

    $scope.ClearText = function () {
        $scope.ShortName = "";
        $scope.LongName = "";
        $scope.District = "";
        $scope.CityID = "";

        $scope.website = "";
        $scope.CountryID = "";
        $scope.Area = "";
        $scope.Fax = "";

        $scope.Address = "";
        $scope.Mobile = "";
        $scope.email = "";
        $scope.Address1 = "";

        $scope.Phone1 = "";
        $scope.Phone = "";
        $scope.Street = "";
        $scope.CreditLimit = "";

        $scope.PriceList = "";
        $scope.Discount = "";
        $scope.CustTypeID = "";
        $scope.CreditDays = "";
        $scope.Region = "";
        
    }
});