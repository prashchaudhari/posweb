﻿app.controller('LoginController', function ($scope, $http, $log, $filter, ngTableParams) {
    

    $scope.Login = function () {
        var username = $scope.username;
        var password = $scope.password;

        if (username == "" || username == null || username== "undefined") {           
            alert("Enter Login User Name");
            $('#username').focus();
            return false;
        }
        if (password == "" || password == null || password == "undefined") {
           
            alert("Enter Password");
            $('#password').focus();
            return false;
        }


        $http({
            url: '../api/Login/LoginDetails?UserName=' + username + '&Password=' + password,
            datatype: 'json',            
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data[0]));
                if (data[0].UserID == 0) {
                    alert("Enter Correct User Name or Password");                   
                }
                else {
                    var UserName = data[0].UserName;
                    var POSCode = data[0].POSCode;
                    var EmployeeID = data[0].EmployeeID;
                    var UserID = data[0].UserID;                   
                   //s alert(UserName);
                    $.ajax({
                        url: '../Login/CheckLogin',                        
                        data: { UserName: UserName, POSCode: POSCode, EmployeeID: EmployeeID, UserID: UserID },
                        type: "POST",
                        cache: false,
                        success: function () {
                            sessionStorage.setItem("UserName", UserName);
                            sessionStorage.setItem("UserID", UserID);
                            sessionStorage.setItem("EmployeeID", EmployeeID);
                            sessionStorage.setItem("POSCode", POSCode);                           

                            
                            window.location.href = '../Dashboard/Dashboard';
                        },
                        error: function () {
                            alert("Error." + data);
                        }
                    });                      
                }
            })
            .error(function (error) {
                alert("Error in Login Detail");
            });

    }
   
});