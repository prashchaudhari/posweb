﻿app.controller("GroupController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.GroupList = function () {

        $http({
            url: '../api/Group/GetGroupList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
              // alert(JSON.stringify(data)); 
                $scope.Group = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Group.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Group, params.orderBy()) : $scope.Group;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.GroupList();


    $scope.AddGroup = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelGroup = function () {
        $scope.panel = true;
    }

    $scope.SubmitGroup = function () {

        if ($scope.Validation() != false) {
            var Groupmaster = {
                "Description": $scope.Description,
                "Code": $scope.GroupCode,
                "DescriptionAR": $scope.DescriptionAR,
                "Note": $scope.Note,
                "GUID": $scope.GUID,
                "Created_by": userid
            };

            $http({
                url: '../api/Group/SaveGroup',
                data: Groupmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.GroupList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditGroup = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.GroupID;
        $scope.Description = rowdata.Description;
        $scope.GroupCode = rowdata.GroupCode;
        $scope.DescriptionAR = rowdata.DescriptionAR;
        $scope.Note = rowdata.Note;
        $scope.GUID = rowdata.GUID;
    }

    $scope.UpdateGroup = function () {
        if ($scope.Validation() != false) {
            var Groupmaster = {
                "GroupID": bid,
                "Description": $scope.Description,
                "Code": $scope.GroupCode,
                "DescriptionAR": $scope.DescriptionAR,
                "Note": $scope.Note,
                "GUID": $scope.GUID,
                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Group/SaveGroup',
                data: Groupmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Group) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.GroupList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteGroup = function (id) {

        var Groupmaster = {
            "GroupID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Group/SaveGroup',
            data: Groupmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.GroupList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.GroupCode == "" || $scope.GroupCode == undefined || $scope.GroupCode == null) {
            $scope.errormessage = "Enter GroupCode";
            return false;
        }
        if ($scope.Description == "" || $scope.Description == undefined || $scope.Description == null) {
            $scope.errormessage = "Enter Description";
            return false;
        }
        if ($scope.DescriptionAR == "" || $scope.DescriptionAR == undefined || $scope.DescriptionAR == null) {
            $scope.errormessage = "Enter Group DescriptionAR";
            return false;
        }

        return true;
    }

    $scope.ClearText = function () {
        $scope.GroupCode = "";
        $scope.Description = "";
        $scope.DescriptionAR = "";
        $scope.Note = "";
        $scope.GUID = "";
        $scope.errormessage = "";
    }
});