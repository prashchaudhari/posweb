﻿app.controller("ComapanyController", function ($scope, $window, $http, $timeout,$log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.CompanyList = function () {

        $http({
            url: '../api/Comapany/GetCompanyList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.Comapany = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Comapany.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Comapany, params.orderBy()) : $scope.Comapany;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
             })
            .error(function (error) {                   
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;
       
    }
    $scope.CompanyList(); 
    
    $scope.GetCityList = function () {

        $http({
            url: '../api/City/GetCityList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.CityList = data;
            })
            .error(function (error) {
               // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });       
    }
    $scope.GetCountryList = function () {

        $http({
            url: '../api/Country/GetCountryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.CountryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    
    
    $scope.AddComapany = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
        $scope.GetCityList();
        $scope.GetCountryList();
    }

    $scope.CancelComapany = function () {
        $scope.panel = true;
    }

    $scope.SubmitComapany = function () {    
        if ($scope.Validation() != false) {

            var master = {
                "Name": $scope.Name,
                "NameAR": $scope.NameAR,
                "Area": $scope.Area,
                "LandMark": $scope.LandMark,

                "District": $scope.District,
                "CityID": $scope.CityID,
                "website": $scope.website,
                "CountryID": $scope.CountryID,

                "Phone": $scope.Phone,
                "Fax": $scope.Fax,
                "ContactPerson": $scope.ContactPerson,
                "Mobile": $scope.Mobile,

                "email": $scope.email,
                "Registered": $scope.Registered
                //"Created_by": userid
            };

            $http({
                url: '../api/Comapany/SaveCompany',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CompanyList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditComapany = function (rowdata) {
        $scope.GetCityList();
        $scope.GetCountryList();
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
       
            bid = rowdata.CompanyID,
                $scope.Name = rowdata.Name,
                $scope.NameAR = rowdata.NameAR,
                $scope.Area = rowdata.Area,
                $scope.LandMark = rowdata.LandMark,

            $scope.District = rowdata.District,
            $scope.CityID = rowdata.CityID,           
            $scope.CountryID = rowdata.CountryID,
                $scope.Phone = rowdata.Phone,

            $scope.Fax = rowdata.Fax,
            $scope.ContactPerson = rowdata.ContactPerson,
            $scope.Mobile = rowdata.Mobile,
                $scope.email = rowdata.email,

                $scope.website = rowdata.website,
                $scope.Registered = rowdata.Registered
    }

    $scope.UpdateComapany = function () {        
        if ($scope.Validation() != false) {
            var master = {
                "CompanyID": bid,
                "Name": $scope.Name,
                "NameAR": $scope.NameAR,
                "Area": $scope.Area,
                "LandMark": $scope.LandMark,

                "District": $scope.District,
                "CityID": $scope.CityID,
                "website": $scope.website,
                "CountryID": $scope.CountryID,

                "Phone": $scope.Phone,
                "Fax": $scope.Fax,
                "ContactPerson": $scope.ContactPerson,
                "Mobile": $scope.Mobile,

                "email": $scope.email,
                "Registered": $scope.Registered,
                "Created_by": userid
            };

            $http({
                url: '../api/Comapany/SaveCompany',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (data) {
                   
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CompanyList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    //$scope.DeleteComapany = function (id) {
        
    //    var branchmaster = {
    //        "BranchID": branchid,
    //        "Deleted_By": userid
    //    };
    //    $http({
    //        url: '../api/Branch/SaveBranch',
    //        data: branchmaster,
    //        datatype: 'json',
    //        method: 'POST',
    //        headers: {
    //            "Content-Type": "application/json",
    //            "accept": "application/json"
    //        }
    //    })
    //        .success(function (branch) {
    //            // alert("Branch Data update Successful");                
    //            //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
    //            $scope.delete = false;
    //            $timeout(function () {
    //                $scope.delete = true;
    //            }, 3000)
    //            $scope.BranchList();
    //        })
    //        .error(function () {
    //            //alert("Error in Save Branch Data");
    //            //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
    //            $scope.error = false;
    //            $timeout(function () {
    //                $scope.error = true;
    //            }, 3000)

    //        })
    //}

    $scope.Validation = function () {
        if ($scope.Name == "" || $scope.Name == undefined || $scope.Name == null)
        {
            $scope.errormessage = "Enter Comapany Name";
            return false;
        }
        if ($scope.NameAR == "" || $scope.NameAR == undefined || $scope.NameAR == null) {
            $scope.errormessage = "Enter NameAR"; return false;
        }
        if ($scope.District == "" || $scope.District == undefined || $scope.District == null) {
            $scope.errormessage = "Enter District"; return false;
        }
        if ($scope.CityID == "" || $scope.CityID == undefined || $scope.CityID == null) {
            $scope.errormessage = "Select City Name"; return false;
        }
        if ($scope.Area == "" || $scope.Area == undefined || $scope.Area == null) {
            $scope.errormessage = "Enter Area"; return false;
        }
        if ($scope.CountryID == "" || $scope.CountryID == undefined || $scope.CountryID == null) {
            $scope.errormessage = "Select Country Name"; return false;
        }       
        return true;
    }

    $scope.ClearText = function ()
    {
        $scope.Name = "";
        $scope.NameAR = "";
        $scope.District = "";
        $scope.CityID = "";

        $scope.website = "";
        $scope.CountryID = "";
        $scope.Phone = "";
        $scope.Fax = "";

        $scope.ContactPerson = "";
        $scope.Mobile = "";
        $scope.email = "";
        $scope.Registered = "";

        $scope.Area = "";
        $scope.LandMark = "";
    }
});