﻿ app.controller("BranchController", function ($scope, $window, $http, $timeout,$log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;  
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.BranchList = function () {

        $http({
            url: '../api/Branch/GetBranchList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.branch = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.branch.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.branch, params.orderBy()) : $scope.branch;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
             })
            .error(function (error) {
                   // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;
       
    }
    $scope.BranchList(); 
    
    $scope.GetCityList = function () {

        $http({
            url: '../api/City/GetCityList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.CityList = data;
            })
            .error(function (error) {
               // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });       
    }
    $scope.GetCountryList = function () {

        $http({
            url: '../api/Country/GetCountryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.CountryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    
    
    $scope.AddBranch = function () {
        $scope.panel = false;
        $('#btnsave').show();
        $('#btnupdate').hide();
        $scope.GetCityList();
        $scope.GetCountryList();
    }

    $scope.CancelBranch = function () {
        $scope.panel = true;
    }

    $scope.SubmitBranch = function () {    

        if ($scope.Validation() != false) {
            var branchmaster = {
                "BranchName": $scope.BranchName,
                "Address": $scope.Address,
                "District": $scope.District,
                "CityID": $scope.CityID,

                "Region": $scope.Region,
                "CountryID": $scope.CountryID,
                "Phone": $scope.Phone,
                "Fax": $scope.Fax,

                "ContactPerson": $scope.ContactPerson,
                "Mobile": $scope.Mobile,
                "email": $scope.email,
                "GPS_Location": $scope.GPS_Location,

                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Branch/SaveBranch',
                data: branchmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.BranchList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditBranch = function (rowdata) {
        $scope.GetCityList();
        $scope.GetCountryList();
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
       
            bid = rowdata.BranchID,
            $scope.BranchName = rowdata.BranchName,
            $scope.Address = rowdata.Address,
            $scope.District = rowdata.District,
            $scope.CityID = rowdata.CityID,

            $scope.Region = rowdata.Region,
            $scope.CountryID = rowdata.CountryID,
            $scope.Phone = rowdata.Phone,
            $scope.Fax = rowdata.Fax,

            $scope.ContactPerson = rowdata.ContactPerson,
            $scope.Mobile = rowdata.Mobile,
            $scope.email = rowdata.email,
            $scope.GPS_Location = rowdata.GPS_Location
    }

    $scope.UpdateBranch = function () {        
        if ($scope.Validation() != false) {
            var branchmaster = {
                "BranchID": bid,
                "BranchName": $scope.BranchName,
                "Address": $scope.Address,
                "District": $scope.District,
                "CityID": $scope.CityID,

                "Region": $scope.Region,
                "CountryID": $scope.CountryID,
                "Phone": $scope.Phone,
                "Fax": $scope.Fax,

                "ContactPerson": $scope.ContactPerson,
                "Mobile": $scope.Mobile,
                "email": $scope.email,
                "GPS_Location": $scope.GPS_Location,

                "Deleted": false,
                "Deleted_By": 0,
                "Created_by": userid
            };

            $http({
                url: '../api/Branch/SaveBranch',
                data: branchmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (branch) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.BranchList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteBranch = function (branchid) {
        
        var branchmaster = {
            "BranchID": branchid,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Branch/SaveBranch',
            data: branchmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (branch) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.BranchList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.BranchName == "" || $scope.BranchName == undefined || $scope.BranchName == null)
        {
            $scope.errormessage = "Enter Branch Name";
            return false;
        }
        if ($scope.Address == "" || $scope.Address == undefined || $scope.Address == null) {
            $scope.errormessage = "Enter Address"; return false;
        }
        if ($scope.District == "" || $scope.District == undefined || $scope.District == null) {
            $scope.errormessage = "Enter District"; return false;
        }
        if ($scope.CityID == "" || $scope.CityID == undefined || $scope.CityID == null) {
            $scope.errormessage = "Select City Name"; return false;
        }
        if ($scope.Region == "" || $scope.Region == undefined || $scope.Region == null) {
            $scope.errormessage = "Enter Region"; return false;
        }
        if ($scope.CountryID == "" || $scope.CountryID == undefined || $scope.CountryID == null) {
            $scope.errormessage = "Select Country Name"; return false;
        }
        if ($scope.Phone == "" || $scope.Phone == undefined || $scope.Phone == null) {
            $scope.errormessage = "Enter Phone"; return false;
        }
        if ($scope.Fax == "" || $scope.Fax == undefined || $scope.Fax == null) {
            $scope.errormessage = "Enter Fax"; return false;
        }
        if ($scope.ContactPerson == "" || $scope.ContactPerson == undefined || $scope.ContactPerson == null) {
            $scope.errormessage = "Enter ContactPerson"; return false;
        }
        if ($scope.Mobile == "" || $scope.Mobile == undefined || $scope.Mobile == null) {
            $scope.errormessage = "Enter Mobile"; return false;
        }
        if ($scope.email == "" || $scope.email == undefined || $scope.email == null) {
            $scope.errormessage = "Enter email"; return false;
        }
        if ($scope.GPS_Location == "" || $scope.GPS_Location == undefined || $scope.GPS_Location == null) {
            $scope.errormessage = "Enter GPS_Location"; return false;
        }
        return true;
    }

    $scope.ClearText = function ()
    {
        $scope.BranchName = "";
        $scope.Address = "";
        $scope.District = "";
        $scope.CityID = "";

        $scope.Region = "";
        $scope.CountryID = "";
        $scope.Phone = "";
        $scope.Fax = "";

        $scope.ContactPerson = "";
        $scope.Mobile = "";
        $scope.email = "";
        $scope.GPS_Location = "";
    }
});