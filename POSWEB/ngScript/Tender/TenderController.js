﻿app.controller("TenderController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.TenderList = function () {

        $http({
            url: '../api/Tender/GetTenderList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.Tender = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Tender.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Tender, params.orderBy()) : $scope.Tender;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.TenderList();   

 
    $scope.AddTender = function () { 
     
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelTender = function () {
        $scope.panel = true;
    }

    $scope.SubmitTender = function () {

        if ($scope.Validation() != false) {
            var Tendermaster = {
                "TenderName": $scope.TenderName,
                "Currency": $scope.Currency,
                "TenderType": $scope.TenderType,
                "ATM_API_Linked": $scope.ATM_API_Linked,
                "Approval_Code": $scope.Approval_Code,
                "Created_by": userid
            };

            $http({
                url: '../api/Tender/SaveTender',
                data: Tendermaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.TenderList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditTender = function (rowdata) {
        //alert(JSON.stringify(rowdata));        
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.TenderID;       
        $scope.TenderName = rowdata.TenderName;
        $scope.Currency = rowdata.Currency;
        $scope.TenderType = rowdata.TenderType;
        $scope.ATM_API_Linked = rowdata.ATM_API_Linked;
        $scope.Approval_Code = rowdata.Approval_Code;
              
    }

    $scope.UpdateTender = function () {
        if ($scope.Validation() != false) {
            var Tendermaster = {
                "TenderID": bid,
                "TenderName": $scope.TenderName,
                "Currency": $scope.Currency,
                "TenderType": $scope.TenderType,
                "ATM_API_Linked": $scope.ATM_API_Linked,
                "Approval_Code": $scope.Approval_Code,               
                "Modified_By": userid
            };

            $http({
                url: '../api/Tender/SaveTender',
                data: Tendermaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Tender) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.TenderList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteTender = function (id) {

        var Tendermaster = {
            "TenderID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Tender/SaveTender',
            data: Tendermaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.TenderList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.TenderName == "" || $scope.TenderName == undefined || $scope.TenderName == null) {
            $scope.errormessage = "Enter Tender Name";
            return false;
        } 
        if ($scope.Currency == "" || $scope.Currency == undefined || $scope.Currency == null) {
            $scope.errormessage = "Enter Currency";
            return false;
        } 
        if ($scope.TenderType == "" || $scope.TenderType == undefined || $scope.TenderType == null) {
            $scope.errormessage = "Enter Name";
            return false;
        } 
        if ($scope.ATM_API_Linked == "" || $scope.ATM_API_Linked == undefined || $scope.ATM_API_Linked == null) {
            $scope.ATM_API_Linked = false;       
        } 
        if ($scope.Approval_Code == "" || $scope.Approval_Code == undefined || $scope.Approval_Code == null) {
            $scope.Approval_Code = false; 
        } 

        return true;
    }

    $scope.ClearText = function () {
        $scope.TenderName = ""; 
        $scope.Currency = ""; 
        $scope.TenderType = ""; 
        $scope.ATM_API_Linked = false; 
        $scope.Approval_Code = false;  
        $scope.errormessage = "";
    }
});