﻿app.controller("PromotionController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $('.pickadate').pickadate();
    $('.pickadate').pickadate({
        
        //format: 'dd-MMM-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        autoclose: true
    });

    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.PromotionList = function () {

        $http({
            url: '../api/Promotion/GetPromotionList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.Comapany = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Comapany.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Comapany, params.orderBy()) : $scope.Comapany;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.PromotionList();

    $scope.GetProductList = function () {

        $http({
            url: '../api/Product/GetProductList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.ProductList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetUOMList = function () {

        $http({
            url: '../api/UOM/GetUOMList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.UOMList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    

    $scope.AddPromotion = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
      
        $scope.GetProductList();
        $scope.GetUOMList();
    }

    $scope.CancelPromotion = function () {
        $scope.panel = true;
    }

    $scope.SubmitPromotion = function () {
        if ($scope.Validation() != false) {

            var master = {
                "EAN": $scope.EAN,
                "ProductID": $scope.ProductID,
                "BaseUnit": $scope.BaseUnit,
                "BaseUnit1": $scope.BaseUnit1,

                "UOMID": $scope.UOMID,
                "Conv": $scope.Conv,
                "Price": $scope.Price,
                "PriceB": $scope.PriceB,

                "PriceC": $scope.PriceC,
                "PriceA": $scope.PriceA, 

                "LeastPrice": $scope.LeastPrice,
                "DiscountPrice": $scope.DiscountPrice,
                "DiscountPercent": $scope.DiscountPercent,
                "SaleStart": $scope.SaleStart,

                "SaleEnd": $scope.SaleEnd,               
                "Created_by": userid
            };

            $http({
                url: '../api/Promotion/SavePromotion',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.PromotionList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditPromotion = function (rowdata) {
        var salestart = ""; var saleend = "";
        $scope.ClearText();
        $scope.GetProductList();
        $scope.GetUOMList();
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
        if (rowdata.SaleStart != "" || rowdata.SaleStart!=null) {
            salestart = rowdata.SaleStart.split('T')[0];
            salestart = getFormattedDate(salestart);
        }
        if (rowdata.SaleEnd != "" || rowdata.SaleEnd != null) {
            saleend = rowdata.SaleEnd.split('T')[0];
            saleend = getFormattedDate(saleend);
        }
        bid = rowdata.AutoID,
            $scope.EAN = rowdata.EAN,
            $scope.ProductID = rowdata.ProductID,
            $scope.UOMID = rowdata.UOMID,
            $scope.Conv = rowdata.Conv,

            $scope.BaseUnit = rowdata.BaseUnit, 
            $scope.PriceC = rowdata.PriceC,

            $scope.Price = rowdata.Price,
            $scope.PriceB = rowdata.PriceB,           
            $scope.PriceA = rowdata.PriceA,

            $scope.LeastPrice = rowdata.LeastPrice,
            $scope.DiscountPercent = rowdata.DiscountPercent,
            $scope.DiscountPrice = rowdata.DiscountPrice,
            
            $scope.SaleEnd = saleend,           
            $scope.SaleStart = salestart
    }

    $scope.UpdatePromotion = function () {
        if ($scope.Validation() != false) {
            var master = {
                "AutoID" : bid,
                "EAN": $scope.EAN,
                "ProductID": $scope.ProductID,
                "BaseUnit": $scope.BaseUnit,

                "UOMID": $scope.UOMID,
                "Conv": $scope.Conv,
                "Price": $scope.Price,
                "PriceB": $scope.PriceB,

                "PriceC": $scope.PriceC,
                "PriceA": $scope.PriceA,

                "LeastPrice": $scope.LeastPrice,
                "DiscountPrice": $scope.DiscountPrice,
                "DiscountPercent": $scope.DiscountPercent,
                "SaleStart": $scope.SaleStart,

                "SaleEnd": $scope.SaleEnd,
                "Created_by": userid
            };

            $http({
                url: '../api/Promotion/SavePromotion',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.PromotionList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeletePromotion = function (id) {

        var master = {
            "AutoID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Promotion/SavePromotion',
            data: master,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (branch) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.PromotionList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.EAN == "" || $scope.EAN == undefined || $scope.EAN == null) {
            $scope.errormessage = "Enter EAN";
            return false;
        }
        if ($scope.ProductID == "" || $scope.ProductID == undefined || $scope.ProductID == null) {
            $scope.errormessage = "Select ProductID"; return false;
        }
        if ($scope.Price == "" || $scope.Price == undefined || $scope.Price == null) {
            $scope.errormessage = "Enter Price"; return false;
        }
        if ($scope.PriceB == "" || $scope.PriceB == undefined || $scope.PriceB == null) {
            $scope.errormessage = "Enter PriceB"; return false;
        }

        if ($scope.UOMID == "" || $scope.UOMID == undefined || $scope.UOMID == null) {
            $scope.errormessage = "Select  UOM"; return false;
        }
        if ($scope.PriceC == "" || $scope.PriceC == undefined || $scope.PriceC == null) {
            $scope.errormessage = "Enter PriceC"; return false;
        }
        if ($scope.BaseUnit == "" || $scope.BaseUnit == undefined || $scope.BaseUnit == null) {
            $scope.errormessage = "Enter BaseUnit";
            return false;
        }      

        if ($scope.Conv == "" || $scope.Conv == undefined || $scope.Conv == null) {
            $scope.errormessage = "Enter Conv"; return false;
        }
        if ($scope.PriceA == "" || $scope.PriceA == undefined || $scope.PriceA == null) {
            $scope.errormessage = "Enter PriceA"; return false;
        }   
        if ($scope.LeastPrice == "" || $scope.LeastPrice == undefined || $scope.LeastPrice == null) {
            $scope.errormessage = "Enter LeastPrice";
            return false;
        }
        if ($scope.DiscountPercent == "" || $scope.DiscountPercent == undefined || $scope.DiscountPercent == null) {
            $scope.errormessage = "Enter Discount Percent"; return false;
        }

        if ($scope.SaleStart == "" || $scope.SaleStart == undefined || $scope.SaleStart == null) {
            $scope.errormessage = "Enter Sale Start"; return false;
        }
        if ($scope.DiscountPrice == "" || $scope.DiscountPrice == undefined || $scope.DiscountPrice == null) {
            $scope.errormessage = "Enter Discount Price"; return false;
        }
        if ($scope.SaleEnd == "" || $scope.SaleEnd == undefined || $scope.SaleEnd == null) {
            $scope.errormessage = "Enter Sale End"; return false;
        }       
        
        return true;
    }

    $scope.ClearText = function () {
        $scope.EAN = "";
        $scope.ProductID = "";
        $scope.Price = "";
        $scope.PriceB = "";

        $scope.DiscountPrice = "";
        $scope.PriceC = "";
        $scope.UOMID = "";
        $scope.LeastPrice = "";

        $scope.BaseUnit = "";
        $scope.DiscountPercent = "";
        $scope.SaleStart = "";  
        $scope.Conv = "";

        $scope.SaleEnd = "";        
        $scope.Discount = "";        
        $scope.PriceA = "";
        

    }

    function getFormattedDate(date) {
        //var t_sdate = "6/1/2012";
        var sptdate = String(date).split("-");
        var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var myMonth = sptdate[1];
        var myDay = sptdate[2];
        var myYear = sptdate[0];
        var combineDatestr = myDay + "-" + months[myMonth - 1] + "-" + myYear;
        return combineDatestr;
        // alert(combineDatestr);
    }
});