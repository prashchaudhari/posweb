﻿app.controller("GoodReceiveController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.GoodReceiveList = function () {

        $http({
            url: '../api/GoodReceive/GetGoodReceiveList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.GoodReceive = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.GoodReceive.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.GoodReceive, params.orderBy()) : $scope.GoodReceive;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.GoodReceiveList();   

    $scope.GetBranchList = function () {

        $http({
            url: '../api/Branch/GetBranchList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.BranchList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.StoreLocationList = function () {

        $http({
            url: '../api/StoreLocation/GetStoreLocationList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.LocationList = data;               
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
 
    $scope.AddGoodReceive = function () { 
        $scope.GetBranchList(); 
        $scope.StoreLocationList();
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelGoodReceive = function () {
        $scope.panel = true;
    }

    $scope.SubmitGoodReceive = function () {

        if ($scope.Validation() != false) {
            var GoodReceivemaster = {
                "TransType": $scope.TransType,
                "BranchID": $scope.BranchID,
                "VendorID": $scope.VendorID,
                "BuyerID": $scope.BuyerID,
                "SLocationID": $scope.SLocationID,
                "POReleaseDate": $scope.POReleaseDate,
                "ReleaseStatus": $scope.ReleaseStatus,
                "FixDiscount": $scope.FixDiscount,
                "SubTotal": $scope.SubTotal,               
                "VAT": $scope.VAT,
                "NetAmount": $scope.NetAmount,
                "PONumber": $scope.PONumber,
                "PODate": $scope.PODate,               
                "Created_by": userid
            };

            $http({
                url: '../api/GoodReceive/SaveGoodReceive',
                data: GoodReceivemaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.GoodReceiveList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditGoodReceive = function (rowdata) {
       // alert(JSON.stringify(rowdata));
        $scope.GetBranchList(); 
        $scope.StoreLocationList();
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.AutoID;       
        $scope.TransType = rowdata.TransType;
        $scope.BranchID = rowdata.BranchID;
        $scope.VendorID = rowdata.VendorID;
        $scope.BuyerID = rowdata.BuyerID;
        $scope.SLocationID = rowdata.SLocationID;
        $scope.POReleaseDate = rowdata.POReleaseDate;
        $scope.ReleaseStatus = rowdata.ReleaseStatus;
        $scope.FixDiscount = rowdata.FixDiscount;
        $scope.SubTotal = rowdata.SubTotal;
        $scope.VAT = rowdata.VAT;
        $scope.NetAmount = rowdata.NetAmount;
        $scope.PONumber = rowdata.PONumber;
        $scope.PODate = rowdata.PODate;
       
    }

    $scope.UpdateGoodReceive = function () {
        if ($scope.Validation() != false) {
            var GoodReceivemaster = {
                "AutoID": bid,
                "TransType": $scope.TransType,
                "BranchID": $scope.BranchID,
                "VendorID": $scope.VendorID,
                "BuyerID": $scope.BuyerID,
                "SLocationID": $scope.SLocationID,
                "POReleaseDate": $scope.POReleaseDate,
                "ReleaseStatus": $scope.ReleaseStatus,
                "FixDiscount": $scope.FixDiscount,
                "SubTotal": $scope.SubTotal,
                "VAT": $scope.VAT,
                "NetAmount": $scope.NetAmount,
                "PONumber": $scope.PONumber,
                "PODate": $scope.PODate,  
                "Modified_By": userid
            };

            $http({
                url: '../api/GoodReceive/SaveGoodReceive',
                data: GoodReceivemaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (GoodReceive) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.GoodReceiveList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteGoodReceive = function (id) {

        var GoodReceivemaster = {
            "AutoID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/GoodReceive/SaveGoodReceive',
            data: GoodReceivemaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.GoodReceiveList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.TransType == "" || $scope.TransType == undefined || $scope.TransType == null) {
            $scope.errormessage = "Enter Trans Type";
            return false;
        } 
        if ($scope.PONumber == "" || $scope.PONumber == undefined || $scope.PONumber == null) {
            $scope.errormessage = "Enter PO Number";
            return false;
        } 
        if ($scope.BranchID == "" || $scope.BranchID == undefined || $scope.BranchID == null) {
            $scope.errormessage = "Select Branch";
            return false;
        } 
        if ($scope.VendorID == "" || $scope.VendorID == undefined || $scope.VendorID == null) {
            $scope.errormessage = "Select Vendor";
            return false;
        } 
        if ($scope.SLocationID == "" || $scope.SLocationID == undefined || $scope.SLocationID == null) {
            $scope.errormessage = "Select Store Location";
            return false;
        } 
        if ($scope.BuyerID == "" || $scope.BuyerID == undefined || $scope.BuyerID == null) {
            $scope.errormessage = "Enter OpeningBalance";
            return false;
        } 
        if ($scope.PODate == "" || $scope.PODate == undefined || $scope.PODate == null) {
            $scope.errormessage = "Enter PODate";
            return false;       
        } 
        if ($scope.ReleaseStatus == "" || $scope.ReleaseStatus == undefined || $scope.ReleaseStatus == null) {
            $scope.ReleaseStatus = false; 
        } 

        return true;
    }

    $scope.ClearText = function () {
        $scope.TransType = ""; 
        $scope.PONumber = ""; 
        $scope.BuyerID = ""; 
        $scope.PODate = ""; 
        $scope.FixDiscount = ""; 
        $scope.ReleaseStatus = false;  
        $scope.SubTotal = "";
        $scope.VAT = "";
        $scope.BranchID = "";
        $scope.VendorID = "";
        $scope.SLocationID = "";
        $scope.POReleaseDate = "";
        $scope.NetAmount = "";
        $scope.errormessage = "";
    }
});