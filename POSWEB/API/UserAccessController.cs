﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class UserAccessController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        public HttpResponseMessage GetUserAccessList(long Id)
        {
            try
            {
                var list = (from useraccess in db.SUserAccesses
                            where useraccess.Deleted==false
                            select new
                            {
                                useraccess.SUser.UserName,
                                useraccess.Branch.BranchName,
                                useraccess.UserID,
                                useraccess.BranchID,
                                useraccess.AllowAccess,
                                useraccess.AutoID

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
        [HttpPost]
        [ActionName("SaveUserAccess")]
        public HttpResponseMessage SaveUserAccess(SUserAccess uaccess)
        {
            try
            {
                if (uaccess != null)
                {
                    if (uaccess.AutoID != 0 && (uaccess.Deleted_By == 0 || uaccess.Deleted_By == null))
                    {
                        SUserAccess b = db.SUserAccesses.Where(x => x.AutoID == uaccess.AutoID).FirstOrDefault();
                        b.UserID = uaccess.UserID;
                        b.BranchID = uaccess.BranchID;
                        b.AllowAccess = uaccess.AllowAccess;
                        b.Modified_Date = DateTime.Now;
                        b.TimeStamp = DateTime.Now.ToString();
                        b.Modified_By = uaccess.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (uaccess.AutoID != 0 && uaccess.Deleted_By != 0)
                    {
                        SUserAccess b = db.SUserAccesses.Where(x => x.AutoID == uaccess.AutoID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = uaccess.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        
                        SUserAccess b = new SUserAccess();
                        b.UserID = uaccess.UserID;
                        b.BranchID = uaccess.BranchID;
                        b.Created_Date = DateTime.Now;
                        b.TimeStamp = DateTime.Now.ToString();
                        b.Created_By = uaccess.Created_By;
                        b.Deleted = false;
                        b.AllowAccess = uaccess.AllowAccess;
                        db.SUserAccesses.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, uaccess);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
