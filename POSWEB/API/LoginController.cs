﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class LoginController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

       [HttpGet]
        public HttpResponseMessage LoginDetails(string UserName, string Password)
        {
            try
            {
                var list = (from user in db.SUsers
                            where user.UserName == UserName && user.Password == Password
                            select new
                            {
                                UserName = user.UserName,
                                POSCode = user.POSCode,
                                RoleID = user.RoleID,
                                EmployeeID = user.EmployeeID,
                                UserID = user.UserID

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
