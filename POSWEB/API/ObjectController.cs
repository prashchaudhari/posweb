﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class ObjectController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetObjectList")]
        public HttpResponseMessage GetObjectList(long Id)
        {
            try
            {
                var list = (from grp in db.Objects
                            where grp.Deleted == false
                            select new
                            {
                                ObjectID = grp.ObjectID,
                                ObjectCaption = grp.ObjectCaption,
                                ObjectType = grp.ObjectType,
                                Group = grp.Group
                               

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveObject")]
        public HttpResponseMessage SaveObject(POSWEB.Models.Object grp)
        {
            try
            {
                if (grp != null)
                {
                    if (grp.ObjectID != 0 && (grp.Deleted_By == 0 || grp.Deleted_By == null))
                    {
                        POSWEB.Models.Object b = db.Objects.Where(x => x.ObjectID == grp.ObjectID).FirstOrDefault();
                        b.ObjectCaption = string.IsNullOrEmpty(grp.ObjectCaption) ? "" : grp.ObjectCaption;
                        b.ObjectType = grp.ObjectType;
                        b.Group = grp.Group;                        
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = grp.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (grp.ObjectID != 0 && grp.Deleted_By != 0)
                    {
                        POSWEB.Models.Object b = db.Objects.Where(x => x.ObjectID == grp.ObjectID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = grp.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        POSWEB.Models.Object b = new POSWEB.Models.Object();
                        b.ObjectCaption = string.IsNullOrEmpty(grp.ObjectCaption) ? "" : grp.ObjectCaption;
                        b.ObjectType = grp.ObjectType;
                        b.Group = grp.Group;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = grp.Created_by;
                        b.Deleted = false;
                        db.Objects.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, grp);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

       
    }
}
