﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class VendorTypeController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetVendorTypeList")]
        public HttpResponseMessage GetVendorTypeList(long Id)
        {
            try
            {
                var list = (from vtype in db.VendorTypes
                            where vtype.Deleted == false
                            select new
                            {
                                VendorTypeID = vtype.VendorTypeID,
                                Description = vtype.Description

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveVendorType")]
        public HttpResponseMessage SaveVendorType(VendorType vtype)
        {
            try
            {
                if (vtype != null)
                {
                    if (vtype.VendorTypeID != 0 && (vtype.Deleted_By == 0 || vtype.Deleted_By == null))
                    {
                        VendorType b = db.VendorTypes.Where(x => x.VendorTypeID == vtype.VendorTypeID).FirstOrDefault();
                        b.Description = vtype.Description;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = vtype.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (vtype.VendorTypeID != 0 && vtype.Deleted_By != 0)
                    {
                        VendorType b = db.VendorTypes.Where(x => x.VendorTypeID == vtype.VendorTypeID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = vtype.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        VendorType b = new VendorType();
                        b.Description = string.IsNullOrEmpty(vtype.Description) ? "" : vtype.Description;
                        b.Created_Date = DateTime.Now;
                        b.Created_By = vtype.Created_By;
                        b.Deleted = false;
                        db.VendorTypes.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, vtype);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
