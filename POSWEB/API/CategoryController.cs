﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CategoryController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        
        public HttpResponseMessage GetCategoryList(long Id)
        {
            try
            {
                var list = (from category in db.Categories
                            where category.Deleted == false
                            select new
                            {
                                CategoryID = category.CategoryID,
                                Description = category.Description

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

       
        public HttpResponseMessage SaveCategory(Category category)
        {
            try
            {
                if (category != null)
                {
                    if (category.CategoryID != 0 && (category.Deleted_By == 0 || category.Deleted_By == null))
                    {
                        Category b = db.Categories.Where(x => x.CategoryID == category.CategoryID).FirstOrDefault();
                        b.Description = string.IsNullOrEmpty(category.Description) ? "" : category.Description;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = category.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (category.CategoryID != 0 && category.Deleted_By != 0)
                    {
                        Category b = db.Categories.Where(x => x.CategoryID == category.CategoryID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = category.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Category b = new Category();
                        b.Description = string.IsNullOrEmpty(category.Description) ? "" : category.Description;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = category.Created_by;
                        b.Deleted = false;
                        db.Categories.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, category);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
