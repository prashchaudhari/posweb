﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class SaleAnalysisReportController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        public HttpResponseMessage GetSale(DateTime fdate, DateTime tdate)
        {
            try
            {
                CustomerSummary cs = new CustomerSummary();
                var list = db.Get_BrandSummary(fdate, tdate).ToList();
                cs.linegraphdata = list.ToArray();
                //var detaillist = db.Get_CustomerSummaryList(fdate, tdate).ToList();
                //cs.datalist = detaillist.ToArray();

                return Request.CreateResponse(HttpStatusCode.OK, cs);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
