﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class TerminalController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
        public HttpResponseMessage GetTerminalList(long Id)
        {
            try
            {
                var list = (from Terminal in db.Terminals
                            join branch in db.Branches on Terminal.BranchID equals branch.BranchID
                            where Terminal.Deleted == false
                            select new
                            {
                                TerminalID = Terminal.TerminalID,
                                TerminalName = Terminal.TerminalName,
                                BranchID = Terminal.BranchID,
                                OpeningBalance = Terminal.OpeningBalance,
                                TerminalType = Terminal.TerminalType,
                                Registered_Key = Terminal.Registered_Key,
                                Registered_Server = Terminal.Registered_Server,
                                BranchName = branch.BranchName
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveTerminal")]
        public HttpResponseMessage SaveTerminal(Terminal subcat)
        {
            try
            {
                if (subcat != null)
                {
                    if (subcat.TerminalID != 0 && (subcat.Deleted_By == 0 || subcat.Deleted_By == null))
                    {
                        Terminal b = db.Terminals.Where(x => x.TerminalID == subcat.TerminalID).FirstOrDefault();
                        b.TerminalName = subcat.TerminalName;
                        b.BranchID = subcat.BranchID;
                        b.OpeningBalance = subcat.OpeningBalance;
                        b.TerminalType = subcat.TerminalType;
                        b.Registered_Key = subcat.Registered_Key;
                        b.Registered_Server = subcat.Registered_Server;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = subcat.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (subcat.TerminalID != 0 && subcat.Deleted_By != 0)
                    {
                        Terminal b = db.Terminals.Where(x => x.TerminalID == subcat.TerminalID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = subcat.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Terminal b = new Terminal();
                        b.TerminalName = subcat.TerminalName;
                        b.BranchID = subcat.BranchID;
                        b.OpeningBalance = subcat.OpeningBalance;
                        b.TerminalType = subcat.TerminalType;
                        b.Registered_Key = subcat.Registered_Key;
                        b.Registered_Server = subcat.Registered_Server;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = subcat.Created_by;
                        b.Deleted = false;
                        db.Terminals.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, subcat);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
