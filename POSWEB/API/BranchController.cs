﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class BranchController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        
        public HttpResponseMessage GetBranchList(long Id)
        {
            try
            {
                var list = (from branch in db.Branches
                            where branch.Deleted == false
                            select new
                            {
                                BranchID = branch.BranchID,
                                BranchName = branch.BranchName,
                                Address = branch.Address,
                                District = branch.District,

                                CityID = branch.CityID,
                                Region = branch.Region,
                                CountryID = branch.CountryID,
                                Phone = branch.Phone,
                                Fax = branch.Fax,

                                GPS_Location =branch.GPS_Location,
                                email = branch.email,
                                Mobile= branch.Mobile,
                                ContactPerson = branch.ContactPerson,
                               
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch(Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        
        public HttpResponseMessage SaveBranch(Branch branch)
        {
            try
            {
                if (branch != null)
                {
                    if (branch.BranchID != 0 && branch.Deleted_By == 0)
                    {
                        Branch b = db.Branches.Where(x => x.BranchID == branch.BranchID).FirstOrDefault();
                        b.BranchName = string.IsNullOrEmpty(branch.BranchName) ? "" : branch.BranchName;
                        b.Address = branch.Address;
                        b.District = branch.District;
                        b.CityID = branch.CityID;
                        b.Region = branch.Region;
                        b.CountryID = branch.CountryID;
                        b.Phone = branch.Phone;
                        b.Fax = branch.Fax;
                        b.ContactPerson = branch.ContactPerson;
                        b.Mobile = branch.Mobile;
                        b.email = branch.email;
                        b.GPS_Location = branch.GPS_Location;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = branch.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if(branch.BranchID != 0 && branch.Deleted_By != 0)
                    {
                        Branch b = db.Branches.Where(x => x.BranchID == branch.BranchID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = branch.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Branch b =new Branch();
                        b.BranchName = string.IsNullOrEmpty(branch.BranchName) ? "" : branch.BranchName;
                        b.Address = branch.Address;
                        b.District = branch.District;
                        b.CityID = branch.CityID;
                        b.Region = branch.Region;
                        b.CountryID = branch.CountryID;
                        b.Phone = branch.Phone;
                        b.Fax = branch.Fax;
                        b.ContactPerson = branch.ContactPerson;
                        b.Mobile = branch.Mobile;
                        b.email = branch.email;
                        b.GPS_Location = branch.GPS_Location;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = branch.Created_by;
                        b.Deleted = branch.Deleted;
                        db.Branches.Add(b);
                        db.SaveChanges();
                       

                        return Request.CreateResponse(HttpStatusCode.OK, branch);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        //[HttpGet]
        //[ActionName("DeleteBranch")]
        //public HttpResponseMessage DeleteBranch(DeleteBranches branch)
        //{
        //    try
        //    {
        //        if (branch.BranchID != null || branch.BranchID != 0)                {
                    
        //                Branch b = db.Branches.Where(x => x.BranchID == branch.BranchID).FirstOrDefault();                        
        //                b.Deleted = true;
        //                b.Deleted_By = branch.Deleted_By;
        //                b.Deleted_Date = DateTime.Now;                       
        //                db.SaveChanges();

        //                return Request.CreateResponse(HttpStatusCode.OK, "success");
                   
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
        //    }
        //}

        public class DeleteBranches
        {
            public int BranchID { get; set; }
            public bool Deleted { get; set; }
            public int Deleted_By { get; set; }
            public DateTime Deleted_Date { get; set; }
        }
    }
}
