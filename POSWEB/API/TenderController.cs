﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class TenderController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
        public HttpResponseMessage GetTenderList(long Id)
        {
            try
            {
                var list = (from Tender in db.Tenders
                            where Tender.Deleted == false
                            select new
                            {
                                TenderID = Tender.TenderID,
                                TenderName = Tender.TenderName,
                                Currency = Tender.Currency,
                                TenderType = Tender.TenderType,
                                ATM_API_Linked = Tender.ATM_API_Linked,
                                Approval_Code = Tender.Approval_Code
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveTender")]
        public HttpResponseMessage SaveTender(Tender subcat)
        {
            try
            {
                if (subcat != null)
                {
                    if (subcat.TenderID != 0 && (subcat.Deleted_By == 0 || subcat.Deleted_By == null))
                    {
                        Tender b = db.Tenders.Where(x => x.TenderID == subcat.TenderID).FirstOrDefault();
                        b.TenderName = subcat.TenderName;
                        b.Approval_Code = subcat.Approval_Code;
                        b.Currency = subcat.Currency;
                        b.TenderType = subcat.TenderType;
                        b.ATM_API_Linked = subcat.ATM_API_Linked;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = subcat.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (subcat.TenderID != 0 && subcat.Deleted_By != 0)
                    {
                        Tender b = db.Tenders.Where(x => x.TenderID == subcat.TenderID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = subcat.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Tender b = new Tender();
                        b.TenderName = subcat.TenderName;
                        b.Approval_Code = subcat.Approval_Code;
                        b.Currency = subcat.Currency;
                        b.TenderType = subcat.TenderType;
                        b.ATM_API_Linked = subcat.ATM_API_Linked;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = subcat.Created_by;
                        b.Deleted = false;
                        db.Tenders.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, subcat);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
