﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class DashboardController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
        public HttpResponseMessage GetGraphData(DateTime fromdate, DateTime todate)
        {
            try
            {
                DashboardCount dc = new DashboardCount();
                var list = db.Get_GraphData(fromdate, todate).ToList();
                dc.graphdata = list.ToArray();

                var cnt = (from c in db.Transactions                           
                            where c.TransactionDate >= fromdate && c.TransactionDate <= todate 
                          select c).ToList();
                
                    var grossale = cnt.Where(x => x.TransType == "SALE").Sum(x => x.NetAmount);
                    var discount = cnt.Where(x => x.TransType == "SALE").Sum(x => x.FixedDiscount);
                    var saletax = cnt.Where(x => x.TransType == "SALE").Sum(x => x.TaxAmount);
                    var retun = cnt.Where(x => x.TransType == "RETURN").Sum(x => x.NetAmount);
                    var cust = cnt.Select(x=>x.CustomerID).Distinct().Count();
                    retun = retun * -1;

                var cnt1 = (from c in db.Transactions
                            join detail in db.TransactionDetails on c.TransactionID equals detail.TransactionID
                            where c.TransactionDate >= fromdate && c.TransactionDate <= todate
                            select detail).ToList();
                var quantity = cnt1.Sum(x => x.Quantity);
                var rquantity = cnt1.Sum(x => x.ReturnQuantity);
                var salecount = quantity - rquantity;
                
                var pur = (from gr in db.GoodRecieves
                           join gre in db.GoodRecieveEntries on gr.PONumber equals gre.PONumber
                           where gr.PODate >= fromdate && gr.PODate <= todate && gr.TransType=="PURCHASE"
                           select gr).ToList().Count();
                var total = (todate - fromdate).TotalDays;
                
                dc.grosssale = grossale; dc.discounts = discount;  dc.salestax = saletax; dc.returns = retun;                
                dc.netsales = grossale - discount - retun;
                dc.salescount = salecount; dc.custcount = cust; dc.avgsale = Convert.ToDouble(String.Format("{0:0.00}", salecount / total)); dc.purchase = pur;
                if(salecount == 0 || total == 0) { dc.avgsale = 0; }
                
                return Request.CreateResponse(HttpStatusCode.OK, dc);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
