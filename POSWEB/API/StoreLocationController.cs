﻿using POSWEB.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class StoreLocationController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
       
        public HttpResponseMessage GetStoreLocationList(long Id)
        {
            try
            {
                var list = (from grp in db.SLocations
                            join brnach in db.Branches on grp.BranchID equals brnach.BranchID
                            join user in db.SUsers on grp.Assigned_User equals user.UserID
                            where grp.Deleted == false
                            select new
                            {
                                SLocationID = grp.SLocationID,
                                Name = grp.Name,
                                NameAR = grp.NameAR,
                                BranchID = grp.BranchID,
                               Assigned_User = grp.Assigned_User,
                                UserName = user.UserName,
                                BranchName = brnach.BranchName


                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        
        public HttpResponseMessage SaveStoreLocation(SLocation sl)
        {
            try
            {
                if (sl != null)
                {
                    if (sl.SLocationID != 0 && (sl.Deleted_By == 0 || sl.Deleted_By == null))
                    {
                        SLocation b = db.SLocations.Where(x => x.SLocationID == sl.SLocationID).FirstOrDefault();
                        b.Name = string.IsNullOrEmpty(sl.Name) ? "" : sl.Name;
                        b.NameAR = sl.NameAR;
                        b.BranchID = sl.BranchID;
                        b.Assigned_User = sl.Assigned_User;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = sl.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (sl.SLocationID != 0 && sl.Deleted_By != 0)
                    {
                        SLocation b = db.SLocations.Where(x => x.SLocationID == sl.SLocationID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = sl.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        SLocation b = new SLocation();
                        b.Name = string.IsNullOrEmpty(sl.Name) ? "" : sl.Name;
                        b.NameAR = sl.NameAR;
                        b.BranchID = sl.BranchID;
                        b.Assigned_User = sl.Assigned_User;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = sl.Created_by;
                        b.Deleted = false;
                        db.SLocations.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, sl);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
