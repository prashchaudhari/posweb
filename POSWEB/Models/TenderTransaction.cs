//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSWEB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TenderTransaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TenderTransaction()
        {
            this.TenderTransactionDetails = new HashSet<TenderTransactionDetail>();
        }
    
        public System.Guid TenderTransactionID { get; set; }
        public System.Guid TransactionID { get; set; }
        public Nullable<int> ServerTransID { get; set; }
        public Nullable<int> BranchID { get; set; }
        public Nullable<int> TerminalID { get; set; }
        public Nullable<System.DateTime> SysDate { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public Nullable<int> CashierID { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> FixedDiscount { get; set; }
        public Nullable<decimal> TaxAmount { get; set; }
        public Nullable<decimal> NetAmount { get; set; }
        public Nullable<decimal> TotalReceived { get; set; }
        public bool SyncStatus { get; set; }
        public string TenderReference { get; set; }
        public System.Guid TenderTransactionIDServer { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TenderTransactionDetail> TenderTransactionDetails { get; set; }
        public virtual Transaction Transaction { get; set; }
    }
}
