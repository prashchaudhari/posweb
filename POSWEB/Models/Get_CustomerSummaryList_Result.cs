//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSWEB.Models
{
    using System;
    
    public partial class Get_CustomerSummaryList_Result
    {
        public string ShortName { get; set; }
        public string CustType { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public Nullable<decimal> TotalPay { get; set; }
        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> Tax { get; set; }
    }
}
