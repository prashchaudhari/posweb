﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSWEB.Models
{
    public class DashboardCount
    {
        public decimal? grosssale { get; set; }
        public decimal? returns { get; set; }
        public decimal? discounts { get; set; }
        public decimal? netsales { get; set; }
        public decimal? salestax { get; set; }
        public double? salescount { get; set; }
        public double? avgsale { get; set; }
        public int? custcount { get; set; }
        public int? purchase { get; set; }
        public Array graphdata { get; set; }

    }
}