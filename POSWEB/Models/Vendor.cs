//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSWEB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vendor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vendor()
        {
            this.GoodRecieves = new HashSet<GoodRecieve>();
            this.PurchaseOrders = new HashSet<PurchaseOrder>();
        }
    
        public int VendorType { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Area { get; set; }
        public string Street { get; set; }
        public string District { get; set; }
        public int CityID { get; set; }
        public string Region { get; set; }
        public int CountryID { get; set; }
        public string Phone { get; set; }
        public string Phone1 { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string website { get; set; }
        public decimal PaymentType { get; set; }
        public decimal CreditDays { get; set; }
        public bool Active { get; set; }
        public string PriceList { get; set; }
        public int Created_By { get; set; }
        public System.DateTime Created_Date { get; set; }
        public Nullable<int> Modified_By { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public bool Deleted { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public int VendorID { get; set; }
    
        public virtual City City { get; set; }
        public virtual Country Country { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GoodRecieve> GoodRecieves { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PurchaseOrder> PurchaseOrders { get; set; }
        public virtual SUser SUser { get; set; }
        public virtual SUser SUser1 { get; set; }
        public virtual VendorType VendorType1 { get; set; }
    }
}
