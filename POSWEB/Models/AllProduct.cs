﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSWEB.Models
{
    public class AllProduct
    {
        public int month { get; set; }
        public int year { get; set; }
        public long BrandID { get; set; }
        public long CategoryID { get; set; }
        public long SubCategoryID { get; set; }
        public long GroupID { get; set; }
    }
}