﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POSWEB.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult CheckLogin(string UserName, string POSCode, string EmployeeID, string UserID)
        {
            Session["UserName"] = UserName;
            Session["POSCode"] = POSCode;
            Session["EmployeeID"] = EmployeeID;
            Session["UserID"] = UserID;
            Session["GateDate"] = DateTime.Now.ToShortDateString();
            return Json("");
        }
        public ActionResult Logout()
        {
            Session["UserName"] = null;
            Session["POSCode"] = null;
            Session["EmployeeID"] = null;
            Session["UserID"] = null; 
            
            return RedirectToAction(Url.Action("Login", "Login"));
        }
    }
}