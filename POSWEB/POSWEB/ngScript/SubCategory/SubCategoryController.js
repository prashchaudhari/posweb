﻿app.controller("SubCategoryController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.SubCategoryList = function () {

        $http({
            url: '../api/SubCategory/GetSubCategoryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.SubCategory = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.SubCategory.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.SubCategory, params.orderBy()) : $scope.SubCategory;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.SubCategoryList();   

    $scope.CategoryList = function () {

        $http({
            url: '../api/Category/GetCategoryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.CategoryList = data;               
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    
    $scope.AddSubCategory = function () { 
        $scope.CategoryList();
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelSubCategory = function () {
        $scope.panel = true;
    }

    $scope.SubmitSubCategory = function () {

        if ($scope.Validation() != false) {
            var SubCategorymaster = {
                "Description": $scope.Description,
                "CategoryID": $scope.CategoryID,
                "Created_by": userid
            };

            $http({
                url: '../api/SubCategory/SaveSubCategory',
                data: SubCategorymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.SubCategoryList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditSubCategory = function (rowdata) {
        alert(JSON.stringify(rowdata));
        $scope.CategoryList();
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.SubCategoryID;
        $scope.CategoryID = rowdata.CategoryID;
        $scope.Description = rowdata.Description;
              
    }

    $scope.UpdateSubCategory = function () {
        if ($scope.Validation() != false) {
            var SubCategorymaster = {
                "SubCategoryID": bid,
                "Description": $scope.Description, 
                "CategoryID": $scope.CategoryID,
                "Modified_By": userid
            };

            $http({
                url: '../api/SubCategory/SaveSubCategory',
                data: SubCategorymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (SubCategory) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.SubCategoryList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteSubCategory = function (id) {

        var SubCategorymaster = {
            "SubCategoryID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/SubCategory/SaveSubCategory',
            data: SubCategorymaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.SubCategoryList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.Description == "" || $scope.Description == undefined || $scope.Description == null) {
            $scope.errormessage = "Enter Name";
            return false;
        }  
        if ($scope.CategoryID == "" || $scope.CategoryID == undefined || $scope.CategoryID == null) {
            $scope.errormessage = "Select Category";
            return false;
        }  

        return true;
    }

    $scope.ClearText = function () {
        $scope.Description = ""; 
        $scope.CategoryID == "";
        $scope.errormessage = "";
    }
});