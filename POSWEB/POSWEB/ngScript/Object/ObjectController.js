﻿app.controller("ObjectController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.ObjectList = function () {

        $http({
            url: '../api/Object/GetObjectList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
              // alert(JSON.stringify(data)); 
                $scope.Object = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Object.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Object, params.orderBy()) : $scope.Object;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.ObjectList();


    $scope.AddObject = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelObject = function () {
        $scope.panel = true;
    }

    $scope.SubmitObject = function () {

        if ($scope.Validation() != false) {
            var Objectmaster = {
                "ObjectCaption": $scope.ObjectCaption,
                "ObjectType": $scope.ObjectType,
                "Group": $scope.Group,
                "Created_by": userid
            };

            $http({
                url: '../api/Object/SaveObject',
                data: Objectmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.ObjectList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditObject = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.ObjectID;
        $scope.ObjectCaption = rowdata.ObjectCaption;
        $scope.ObjectType = rowdata.ObjectType;
        $scope.Group = rowdata.Group;      
    }

    $scope.UpdateObject = function () {
        if ($scope.Validation() != false) {
            var Objectmaster = {
                "ObjectID": bid,
                "ObjectCaption": $scope.ObjectCaption,
                "ObjectType": $scope.ObjectType,
                "Group": $scope.Group,
               
                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Object/SaveObject',
                data: Objectmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Object) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.ObjectList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteObject = function (id) {

        var Objectmaster = {
            "ObjectID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Object/SaveObject',
            data: Objectmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.ObjectList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.Group == "" || $scope.Group == undefined || $scope.Group == null) {
            $scope.errormessage = "Enter Group";
            return false;
        }
        if ($scope.ObjectType == "" || $scope.ObjectType == undefined || $scope.ObjectType == null) {
            $scope.errormessage = "Enter Object Type";
            return false;
        }
        if ($scope.ObjectCaption == "" || $scope.ObjectCaption == undefined || $scope.ObjectCaption == null) {
            $scope.errormessage = "Enter Object Object Caption";
            return false;
        }

        return true;
    }

    $scope.ClearText = function () {
        $scope.Group = "";
        $scope.ObjectType = "";
        $scope.ObjectCaption = "";       
    }
});