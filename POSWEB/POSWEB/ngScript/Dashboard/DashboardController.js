﻿
app.controller("DashboardController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);

    $('.pickadate').pickadate({
        format: 'dd-mmm-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        autoclose: true
    });
    var currdate = new Date();
    var day = currdate.getDate();
    var month = currdate.getMonth()+1;
    var year = currdate.getFullYear();
    currdate = year + '/' + month + '/' + day;

    $scope.showdata = function () {
        fromdate = $scope.From;
        todate = $scope.To;
        if (fromdate == 'undefined' || fromdate == "" || fromdate == null)
        {
            fromdate = currdate;            
        }
        if (todate == 'undefined' || todate == "" || todate == null) {            
            todate = currdate;
        }
        var Combined = new Array();
        $http({
            url: '../api/Dashboard/GetGraphData?fromdate='+fromdate+'&todate='+todate,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
               // alert(JSON.stringify(response));
                //console.log(response[0].count);
                $scope.grosssale = response.grosssale; $scope.return = response.returns;
                $scope.discount = response.discounts; $scope.netsale = response.netsales;
                $scope.saletax = response.salestax; $scope.salecount = response.salescount;
                $scope.avgsale = response.avgsale; $scope.custcount = response.custcount;
                $scope.purchase = response.purchase;

                Combined[0] = ['CODE', 'Purchase', 'Sale'];
                for (var i = 0; i < response.graphdata.length; i++) {
                    Combined[i + 1] = [response.graphdata[i].CODE, response.graphdata[i].purchcount, response.graphdata[i].count];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options_column = {
                    fontName: 'Roboto',
                    height: 350,
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 300
                    },
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Sales and Purchase',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var column = new google.visualization.ColumnChart($('#google-column')[0]);
                column.draw(data, options_column);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });

        
    }
    $scope.showdata();
    
});
//// Data
        //var data = google.visualization.arrayToDataTable([
        //    ['Product', 'Sales', 'Purchase'],
        //    ['0', 1, 0],
        //    ['1', 4, 0],
        //    ['10880', 0, 3],
        //    ['10884', 0, 10]
        //]);


        //// Options
        //var options_column = {
        //    fontName: 'Roboto',
        //    height: 400,
        //    fontSize: 12,
        //    chartArea: {
        //        left: '5%',
        //        width: '90%',
        //        height: 350
        //    },
        //    tooltip: {
        //        textStyle: {
        //            fontName: 'Roboto',
        //            fontSize: 13
        //        }
        //    },
        //    vAxis: {
        //        title: 'Sales and Purchase',
        //        titleTextStyle: {
        //            fontSize: 13,
        //            italic: false
        //        },
        //        gridlines: {
        //            color: '#e5e5e5',
        //            count: 10
        //        },
        //        minValue: 0
        //    },
        //    legend: {
        //        position: 'top',
        //        alignment: 'center',
        //        textStyle: {
        //            fontSize: 12
        //        }
        //    }
        //};

        //// Draw chart
        //var column = new google.visualization.ColumnChart($('#google-column')[0]);
        //column.draw(data, options_column);