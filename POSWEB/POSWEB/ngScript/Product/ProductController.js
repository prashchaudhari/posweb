﻿app.directive('uploadFiles', function () {
    return {
        scope: true,        //create a new scope  
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element  
                for (var i = 0; i < files.length; i++) {
                    //emit event upward  
                    scope.$emit("seletedFile", { file: files[i] });
                }
            });
        }
    };
});
app.controller("ProductController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true;
    $scope.delete = true; $scope.pastimage = true;

    //1. Used to list all selected files  
    $scope.files = [];

    ////2. a simple model that want to pass to Web API along with selected files  
    //var jsonData = {
    //    name: "Jignesh Trivedi",
    //    comments: "Multiple upload files"
    //};
    //3. listen for the file selected event which is raised from directive  
    $scope.$on("seletedFile", function (event, args) {
        $scope.$apply(function () {
            //add the file object to the scope's files collection  
            $scope.files.push(args.file);
        });
    });  


    $scope.ProductList = function () {

        $http({
            url: '../api/Product/GetProductList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.Product = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Product.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Product, params.orderBy()) : $scope.Product;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
             })
            .error(function (error) {                   
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;
       
    }
    $scope.ProductList(); 

    $scope.GetProductTypeList = function () {

        $http({
            url: '../api/ProductType/GetProductTypeList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.ProductTypeList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetBrandList = function () {

        $http({
            url: '../api/Brand/GetBrandList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.BrandList = data;
            })
            .error(function (error) {
               // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });       
    }
    $scope.GetCategoryList = function () {

        $http({
            url: '../api/Category/GetCategoryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.CategoryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetUOMList = function () {

        $http({
            url: '../api/UOM/GetUOMList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.UOMList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetGroupList = function () {

        $http({
            url: '../api/Group/GetGroupList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.GroupList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetTaxesList = function () {

        $http({
            url: '../api/Taxes/GetTaxesList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.TaxesList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetSubCategoryList = function () {

        $http({
            url: '../api/SubCategory/GetSubCategoryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.SubCategoryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    
    
    $scope.AddProduct = function () {
        $scope.panel = false;
        $scope.pastimage = true;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
        $scope.GetProductTypeList();
        $scope.GetBrandList();
        $scope.GetCategoryList();
        $scope.GetUOMList();
        $scope.GetGroupList();
        $scope.GetTaxesList();
        $scope.GetSubCategoryList();
    }

    $scope.CancelProduct = function () {
        $scope.panel = true;
    }

    ////4. Post data and selected files.  
    //$scope.SubmitProduct = function () {

    //    var models = {
    //        "ProductID": 0,
    //            "ProductTypeID": $scope.ProductTypeID,
    //            "Description": $scope.Description,
    //            "DescriptionAR": $scope.DescriptionAR,
    //            "SubDescription1": $scope.SubDescription1,
    //            "SubDescription2": $scope.SubDescription2,
    //            "Size": $scope.Size,
    //            "ShowOnHomePage": $scope.ShowOnHomePage,
    //            "BrandID": $scope.BrandID,
    //            "CategoryID": $scope.CategoryID,
    //            "SubCategoryID": $scope.SubCategoryID,
    //            "GroupID": $scope.GroupID,
    //            "Model": $scope.Model,
    //            "TaxID": $scope.TaxID,
    //            "BaseUnitID": $scope.UOMID,
    //            "Cost": $scope.Cost,
    //            "Note": $scope.Note,
    //            "CODE": $scope.CODE,
    //            "proimg": $scope.proimg,
    //            //"Deleted_By":0,
    //            "Created_by": userid
    //        };

    //    $http({
    //        method: 'POST',
    //        url: 'http://localhost:53571/SaveProduct',
    //        headers: { 'Content-Type': undefined },

    //        transformRequest: function (data) {
    //            var formData = new FormData();
    //            formData.append("model", angular.toJson(data.model));
    //            for (var i = 0; i < data.files.length; i++) {
    //                formData.append("file" + i, data.files[i]);
    //            }
    //            return formData;
    //        },
    //        data: { model: models, files: $scope.files }
    //    }).
    //        success(function (data, status, headers, config) {
    //            alert("success!");
    //        }).
    //        error(function (data, status, headers, config) {
    //            alert("failed!");
    //        });
    //};  

    $scope.SubmitProduct = function () {       
        if ($scope.Validation() != false) {

            var models = {
                "ProductID": 0,
                "ProductTypeID": $scope.ProductTypeID,
                "Description": $scope.Description,
                "DescriptionAR": $scope.DescriptionAR,
                "SubDescription1": $scope.SubDescription1,
                "SubDescription2": $scope.SubDescription2,
                "Size": $scope.Size,
                "ShowOnHomePage": $scope.ShowOnHomePage,
                "BrandID": $scope.BrandID,
                "CategoryID": $scope.CategoryID,
                "SubCategoryID": $scope.SubCategoryID,
                "GroupID": $scope.GroupID,
                "Model": $scope.Model,
                "TaxID": $scope.TaxID,
                "BaseUnitID": $scope.UOMID,
                "Cost": $scope.Cost,
                "Note": $scope.Note,
                "CODE": $scope.CODE,
                //"proimg": $scope.proimg,
                //"Deleted_By":0,
                "Created_by": userid
            };

            $http({
                method: 'POST',
                url: 'http://localhost:53574/SaveProduct',
                headers: { 'Content-Type': undefined },

                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i]);
                    }
                    return formData;
                },
                data: { model: models, files: $scope.files }
            })
                .success(function (state) {
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.ProductList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })
            $scope.files = [];
            $scope.panel = true;
        }
    }

    $scope.EditProduct = function (rowdata) {
        $scope.ClearText();
        $scope.GetProductTypeList();
        $scope.GetBrandList();
        $scope.GetCategoryList();
        $scope.GetUOMList();
        $scope.GetGroupList();
        $scope.GetTaxesList();
        $scope.GetSubCategoryList();
        $scope.panel = false;
        $scope.pastimage = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
       
        bid = rowdata.ProductID,
        $scope.ProductTypeID = rowdata.ProductTypeID,
        $scope.Description = rowdata.Description,
        $scope.DescriptionAR = rowdata.DescriptionAR,
        $scope.SubDescription1 = rowdata.SubDescription1,
        $scope.SubDescription2 = rowdata.SubDescription2,
        $scope.Size = rowdata.Size,
        $scope.ShowOnHomePage = rowdata.ShowOnHomePage,
        $scope.BrandID = rowdata.BrandID,
        $scope.CategoryID = rowdata.CategoryID,
        $scope.SubCategoryID = rowdata.SubCategoryID,
        $scope.GroupID = rowdata.GroupID,
        $scope.Model = rowdata.Model,
            $scope.TaxID = rowdata.TaxID,
            $scope.UOMID = rowdata.BaseUnitID,
            $scope.Cost = rowdata.Cost,
            $scope.Note = rowdata.Note,
            $scope.CODE = rowdata.CODE,
            $scope.proimg = rowdata.proimg
            document.getElementById("pimage").src = rowdata.proimg;
    }

    $scope.UpdateProduct = function () {        
        if ($scope.Validation() != false) {
            var models = {
                "ProductID": bid,
                "ProductTypeID": $scope.ProductTypeID,
                "Description": $scope.Description,
                "DescriptionAR": $scope.DescriptionAR,
                "SubDescription1": $scope.SubDescription1,
                "SubDescription2": $scope.SubDescription2,
                "Size": $scope.Size,
                "ShowOnHomePage": $scope.ShowOnHomePage,
                "BrandID": $scope.BrandID,
                "CategoryID": $scope.CategoryID,
                "SubCategoryID": $scope.SubCategoryID,
                "GroupID": $scope.GroupID,
                "Model ": $scope.Model,
                "TaxID": $scope.TaxID,
                "BaseUnitID": $scope.UOMID,
                "Cost": $scope.Cost,
                "Note": $scope.Note,
                "CODE": $scope.CODE,
                "proimg": $scope.proimg,
                "Deleted_By": 0,
                "Created_by": userid
            };

            $http({
                method: 'POST',
                url: 'http://localhost:53574/SaveProduct',
                headers: { 'Content-Type': undefined },

                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.model));
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i]);
                    }
                    return formData;
                },
                data: { model: models, files: $scope.files }
            })
                .success(function (data) {
                   
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.ProductList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })
            $scope.files = [];
            $scope.panel = true;
        }
    }

    $scope.DeleteProduct = function (id) {
        
        var branchmaster = {
            "ProductID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Product/UpdateProduct',
            data: branchmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (branch) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.ProductList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.ProductTypeID == "" || $scope.ProductTypeID == undefined || $scope.ProductTypeID == null)
        {
            $scope.errormessage = "Select Product Type";
            return false;
        }
        if ($scope.BrandID == "" || $scope.BrandID == undefined || $scope.BrandID == null) {
            $scope.errormessage = "Select Brand";
            return false;
        }
        if ($scope.CategoryID == "" || $scope.CategoryID == undefined || $scope.CategoryID == null) {
            $scope.errormessage = "Select Category";
            return false;
        }
        if ($scope.SubCategoryID == "" || $scope.SubCategoryID == undefined || $scope.SubCategoryID == null) {
            $scope.errormessage = "Select Sub Category";
            return false;
        }
        if ($scope.GroupID == "" || $scope.GroupID == undefined || $scope.GroupID == null) {
            $scope.errormessage = "Select Group";
            return false;
        }
        if ($scope.TaxID == "" || $scope.TaxID == undefined || $scope.TaxID == null) {
            $scope.errormessage = "Select Tax";
            return false;
        }
        if ($scope.UOMID == "" || $scope.UOMID == undefined || $scope.UOMID == null) {
            $scope.errormessage = "Select Base Unit";
            return false;
        }
        if ($scope.CODE == "" || $scope.CODE == undefined || $scope.CODE == null) {
            $scope.errormessage = "Enter unique Code";
            return false;
        }
        if ($scope.ShowOnHomePage == "" || $scope.ShowOnHomePage == undefined || $scope.ShowOnHomePage == null) {
            
            $scope.ShowOnHomePage = false;
        }
        if ($scope.Cost == "" || $scope.Cost == undefined || $scope.Cost == null) {

            $scope.Cost = 0;
        }

        return true;
    }

    $scope.ClearText = function ()
    {
        $("#file_upload").replaceWith($("#file_upload").val('').clone(true));
        $scope.ProductTypeID = "";
        $scope.Description = "";
        $scope.DescriptionAR = "";
        $scope.SubDescription1 = "";
        $scope.SubDescription2 = "";
        $scope.Size = "";
        $scope.ShowOnHomePage = "";
        $scope.BrandID = "";
        $scope.CategoryID = "";
        $scope.SubCategoryID = "";
        $scope.GroupID = "";
        $scope.Model = "";
        $scope.TaxID = "";
        $scope.BaseUnitID = "";
        $scope.Cost = "";
        $scope.Note = "";
        $scope.CODE = "";        
       
        
    }
});
