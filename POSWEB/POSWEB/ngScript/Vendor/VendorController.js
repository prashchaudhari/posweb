﻿app.controller("VendorController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {
   
    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.VendorList = function () {

        $http({
            url: '../api/Vendor/GetVendorList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })  
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.branch = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.branch.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.branch, params.orderBy()) : $scope.branch;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                
                //alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.VendorList();
    $scope.GetCityList = function () {

        $http({
            url: '../api/City/GetCityList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.CityList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetCountryList = function () {

        $http({
            url: '../api/Country/GetCountryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));   
                $scope.CountryList = data;
            })
            .error(function (error) {
                //alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetVendorTypeList = function () {

        $http({
            url: '../api/VendorType/GetVendorTypeList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.VendorTypeList = data;
                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });       
    }
   
       $scope.AddVendor = function () {        
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
        $scope.GetVendorTypeList();
        $scope.GetCountryList();
        $scope.GetCityList();
    }

    $scope.CancelVendor = function () {
        $scope.panel = true;
    }

    $scope.SubmitVendor = function () {
     
        if ($scope.Validation() != false) {
            var Vendormaster = {
                "VendorType": $scope.VendorTypeID,
                "ShortName": $scope.ShortName,
                "LongName": $scope.LongName,
                "Address": $scope.Address,
                "Address1": $scope.Address1,
                "CityID": $scope.CityID,
                "CountryID": $scope.CountryID,
                "Area": $scope.Area,
                "Street": $scope.Street,
                "District": $scope.District,
                "Region": $scope.Region,
                "Phone": $scope.Phone,
                "Phone1": $scope.Phone1,
                "Fax": $scope.Fax,
                "Mobile": $scope.Mobile,
                "Email": $scope.Email,
                "website": $scope.website,
                "PaymentType": $scope.PaymentType,
                "CreditDays": $scope.CreditDays,
                "Active": $scope.Active,
                "PriceList": $scope.PriceList,
                "Created_by": userid
            };

            $http({
                url: '../api/Vendor/SaveVendor',
                data: Vendormaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.VendorList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditVendor = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.GetVendorTypeList();
        $scope.GetCountryList();
        $scope.GetCityList();
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

            bid = rowdata.VendorID,
                $scope.VendorTypeID = rowdata.VendorTypeID,
                $scope.ShortName = rowdata.ShortName,
                $scope.LongName = rowdata.LongName,
                $scope.Address = rowdata.Address,
                $scope.Address1 = rowdata.Address1,
                $scope.CityID = rowdata.CityID,
                $scope.CountryID = rowdata.CountryID,
                $scope.Area = rowdata.Area,
                $scope.Street = rowdata.Street,
                $scope.District = rowdata.District,
                $scope.Region = rowdata.Region,
                $scope.Phone = rowdata.Phone,
                $scope.Phone1 = rowdata.Phone1,
                $scope.Fax = rowdata.Fax,
                $scope.Mobile = rowdata.Mobile,
                $scope.Email = rowdata.Email,
                $scope.website = rowdata.website,
                $scope.PaymentType = rowdata.PaymentType,
                $scope.CreditDays = rowdata.CreditDays,
                $scope.Active = false,
                $scope.PriceList = rowdata.PriceList
    }
    
    $scope.UpdateVendor = function () {
        if ($scope.Validation() != false) {
            var Vendormaster = {
              "VendorType":  $scope.VendorTypeID,
              "ShortName":  $scope.ShortName ,
              "LongName":$scope.LongName ,
              "Address":$scope.Address ,
              "Address1":$scope.Address1 ,
              "CityID":$scope.CityID,
              "CountryID":$scope.CountryID,
              "Area":$scope.Area ,
              "Street":$scope.Street ,
              "District":$scope.District,
              "Region":$scope.Region ,
              "Phone":$scope.Phone ,
              "Phone1":$scope.Phone1 ,
              "Fax":$scope.Fax,
              "Mobile":$scope.Mobile ,
              "Email":$scope.Email ,
              "website":$scope.website ,
              "PaymentType":$scope.PaymentType ,
              "CreditDays":$scope.CreditDays ,
              "Active":$scope.Active,
              "PriceList":$scope.PriceList ,
              "VendorID":bid,
                "Deleted": false,
                "Created_By": userid
            };

            $http({
                url: '../api/Vendor/SaveVendor',
                data: Vendormaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Vendor) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.VendorList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteVendor = function (Vendorid) {

        var Vendormaster = {
            "VendorID": Vendorid,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Vendor/SaveVendor',
            data: Vendormaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (Vendor) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.VendorList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.ShortName == "" || $scope.ShortName == undefined || $scope.ShortName == null) {
            $scope.errormessage = "Enter Short Name ";
            return false;
        }
        if ($scope.LongName == "" || $scope.LongName == undefined || $scope.LongName == null) {
            $scope.errormessage = "Enter Long Name ";
            return false;
        }
        if ($scope.VendorTypeID == "" || $scope.VendorTypeID == undefined || $scope.VendorTypeID == null) {
            $scope.errormessage = "Select Vendor Type ";
            return false;
        }
        if ($scope.CityID == "" || $scope.CityID == undefined || $scope.CityID == null) {
            $scope.errormessage = "Select City ";
            return false;
        }
        if ($scope.CountryID == "" || $scope.CountryID == undefined || $scope.CountryID == null) {
            $scope.errormessage = "Select Country ";
            return false;
        }
        return true;
    }

    $scope.ClearText = function () {
        $scope.VendorTypeID = "";
        $scope.ShortName = "";
        $scope.LongName = "";
        $scope.Address = "";
        $scope.Address1 = "";
        $scope.CityID = "";
        $scope.CountryID = "";
        $scope.Area = "";
        $scope.street = "";
        $scope.District = "";
        $scope.Region = "";
        $scope.Phone = "";
        $scope.Phone1 = "";
        $scope.Fax = "";
        $scope.Mobile = "";
        $scope.Email = "";
        $scope.website = "";
        $scope.PaymentType = "";
        $scope.CreditDays = "";
        $scope.Active = false;
        $scope.PriceList = "";



    }
});