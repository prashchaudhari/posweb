﻿app.controller("StoreLocationController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.StoreLocationList = function () {

        $http({
            url: '../api/StoreLocation/GetStoreLocationList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.StoreLocation = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.StoreLocation.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.StoreLocation, params.orderBy()) : $scope.StoreLocation;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.StoreLocationList();

    $scope.GetBranchList = function () {

        $http({
            url: '../api/Branch/GetBranchList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.BranchList = data;               
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }    
    $scope.GetSUserList = function () {

        $http({
            url: '../api/SUser/GetSUserList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.UserList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    

    $scope.AddStoreLocation = function () {
        $scope.GetBranchList(); 
        $scope.GetSUserList();
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelStoreLocation = function () {
        $scope.panel = true;
    }

    $scope.SubmitStoreLocation = function () {

        if ($scope.Validation() != false) {
            var StoreLocationmaster = {
                "Name": $scope.Name,
                "Assigned_User": $scope.UserID,
                "BranchID": $scope.BranchID,
                "NameAR": $scope.NameAR,               
                "Created_by": userid
            };

            $http({
                url: '../api/StoreLocation/SaveStoreLocation',
                data: StoreLocationmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.StoreLocationList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditStoreLocation = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.GetBranchList();
        $scope.GetSUserList();
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.SLocationID;
        $scope.Name = rowdata.Name;
        $scope.UserID = rowdata.Assigned_User;
        $scope.BranchID = rowdata.BranchID;
        $scope.NameAR = rowdata.NameAR;        
    }

    $scope.UpdateStoreLocation = function () {
        if ($scope.Validation() != false) {
            var StoreLocationmaster = {
                "SLocationID": bid,
                "Name": $scope.Name,
                "Assigned_User": $scope.UserID,
                "BranchID": $scope.BranchID,
                "NameAR": $scope.NameAR,
                "Modified_By": userid
            };

            $http({
                url: '../api/StoreLocation/SaveStoreLocation',
                data: StoreLocationmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (StoreLocation) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.StoreLocationList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteStoreLocation = function (id) {

        var StoreLocationmaster = {
            "SLocationID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/StoreLocation/SaveStoreLocation',
            data: StoreLocationmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.StoreLocationList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.Name == "" || $scope.Name == undefined || $scope.Name == null) {
            $scope.errormessage = "Enter Name";
            return false;
        }
        if ($scope.NameAR == "" || $scope.NameAR == undefined || $scope.NameAR == null) {
            $scope.errormessage = "Enter NameAR";
            return false;
        } 
        if ($scope.BranchID == "" || $scope.BranchID == undefined || $scope.BranchID == null) {
            $scope.errormessage = "Select Branch";
            return false;
        } 
        if ($scope.UserID == "" || $scope.UserID == undefined || $scope.UserID == null) {
            $scope.errormessage = "Select User";
            return false;
        } 

        return true;
    }

    $scope.ClearText = function () {
        $scope.Name = "";
        $scope.UserID = "";
        $scope.BranchID = "";
        $scope.NameAR = "";        
    }
});