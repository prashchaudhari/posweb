﻿app.controller("BrandController", function ($scope, $window, $http, $timeout,$log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.BrandList = function () {

        $http({
            url: '../api/Brand/GetBrandList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.branch = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.branch.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.branch, params.orderBy()) : $scope.branch;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
             })
            .error(function (error) {
                   // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;
       
    }
    $scope.BrandList();    
   
    
    $scope.AddBrand = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();      
    }

    $scope.CancelBrand = function () {
        $scope.panel = true;
    }

    $scope.SubmitBrand = function () {
      
        if ($scope.Validation() != false) {
            var brandmaster = {
                "Description": $scope.Description,
                "ColorCode": $scope.Ucolor, 
                "Created_by": userid
            };

            $http({
                url: '../api/Brand/SaveBrand',
                data: brandmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.BrandList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditBrand = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
       
        bid = rowdata.BrandID,
            $scope.Description = rowdata.Description,
            $scope.Ucolor = rowdata.ColorCode
            
    }

    $scope.UpdateBrand = function () {        
        if ($scope.Validation() != false) {
            var brandmaster = {
                "BrandID": bid,
                "Description": $scope.Description,                
                "ColorCode": $scope.Ucolor,  
                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Brand/SaveBrand',
                data: brandmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (brand) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.BrandList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteBrand = function (brandid) {
        
        var brandmaster = {
            "BrandID": brandid,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Brand/SaveBrand',
            data: brandmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (brand) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.BrandList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.Description == "" || $scope.Description == undefined || $scope.Description == null)
        {
            $scope.errormessage = "Enter Brand Description";
            return false;
        }
        if ($scope.Ucolor == "" || $scope.Ucolor == undefined || $scope.Ucolor == null) {
            $scope.errormessage = "Select Brand Color";
            return false;
        }
        
        return true;
    }

    $scope.ClearText = function ()
    {
        $scope.Description = "";
        $scope.Ucolor = "";
       
    }
});