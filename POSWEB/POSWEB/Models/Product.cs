//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSWEB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            this.Conversi0n = new HashSet<Conversi0n>();
            this.Conversions = new HashSet<Conversion>();
            this.Prices = new HashSet<Price>();
        }
    
        public int ProductTypeID { get; set; }
        public string Description { get; set; }
        public string DescriptionAR { get; set; }
        public string SubDescription1 { get; set; }
        public string SubDescription2 { get; set; }
        public string Size { get; set; }
        public Nullable<bool> ShowOnHomePage { get; set; }
        public Nullable<int> BrandID { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public Nullable<int> SubCategoryID { get; set; }
        public Nullable<int> GroupID { get; set; }
        public string Model { get; set; }
        public Nullable<int> TaxID { get; set; }
        public Nullable<int> BaseUnitID { get; set; }
        public Nullable<decimal> Cost { get; set; }
        public Nullable<int> Created_By { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public Nullable<int> Modified_by { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
        public Nullable<bool> Deleted { get; set; }
        public Nullable<int> Deleted_By { get; set; }
        public Nullable<System.DateTime> Deleted_Date { get; set; }
        public string Note { get; set; }
        public int ProductID { get; set; }
        public string CODE { get; set; }
        public string proimg { get; set; }
    
        public virtual Brand Brand { get; set; }
        public virtual Category Category { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversi0n> Conversi0n { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversion> Conversions { get; set; }
        public virtual Group Group { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Price> Prices { get; set; }
        public virtual ProductType ProductType { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual UOM UOM { get; set; }
        public virtual SUser SUser { get; set; }
        public virtual SUser SUser1 { get; set; }
        public virtual Tax Tax { get; set; }
    }
}
