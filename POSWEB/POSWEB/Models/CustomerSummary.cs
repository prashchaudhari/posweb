﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSWEB.Models
{
    public class CustomerSummary
    {
        public Array linegraphdata { get; set; }
        public Array datalist { get; set; }
    }
}