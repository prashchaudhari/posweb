﻿$(document).ready(function () {
    alert('');
    $("#branch").change(function () {
        //var id = $("#Id").val();
        var booksDiv = $("#contentdata");
        $.ajax({
            cache: false,
            type: "GET",
            url: "@(Url.Content('/Branches/Index'))",
            data: {  },
            success: function (data) {
                booksDiv.html('');
                booksDiv.html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert('Failed to retrieve Branch.');
            }
        });
    });
});        