﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace POSWEB.Models
{
    public class SaleSummary
    {
        public decimal? tabxablesales { get; set; }
        public decimal? nontaxablesales { get; set; }
        public decimal? totalproductsales { get; set; }
        public decimal? itemdiscount { get; set; }
        public decimal? netsales { get; set; }
        public double? accepted { get; set; }
        public double? hold { get; set; }
        public int? returns { get; set; }
        public decimal? acceptedamt { get; set; }
        public decimal? holdamt { get; set; }
        public decimal? returnsamt { get; set; }
        public Array linegraphdata { get; set; }
    }
}