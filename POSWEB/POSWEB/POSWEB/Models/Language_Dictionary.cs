//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POSWEB.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Language_Dictionary
    {
        public string English { get; set; }
        public string Arabic { get; set; }
        public string French { get; set; }
        public string Greek { get; set; }
        public string Spanish { get; set; }
        public string Hindi { get; set; }
        public string Urdu { get; set; }
        public int DictID { get; set; }
    }
}
