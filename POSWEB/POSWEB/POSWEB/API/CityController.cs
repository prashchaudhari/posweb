﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class CityController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        public HttpResponseMessage GetCityList(long Id)
        {
            try
            {
                var list = (from city in db.Cities                            
                            select new
                            {
                                CityID = city.CityID,
                                Name = city.Name
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch(Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,new{ Error = ex.InnerException.Message });
            }
        }
    }
}
