﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class UserController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetUserList")]
        public HttpResponseMessage GetUserList(long Id)
        {
            try
            {
                var list = (from user in db.SUsers
                            where user.Deleted == false
                            select new
                            {
                                user.RoleID,
                                user.EmployeeID,
                                user.UserName,
                                user.POSCode,
                                user.Password,
                                user.UserID

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
