﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class ComapanyController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

       
        public HttpResponseMessage GetCompanyList(long Id)
        {
            try
            {
                var list = (from company in db.Comapanies
                           join city in db.Cities on company.CityID equals city.CityID
                           join contry in db.Countries on company.CountryID equals contry.CountryID
                            select new
                            {
                                CompanyID = company.CompanyID,
                                Name = company.Name,
                                NameAR = company.NameAR,
                                LandMark = company.LandMark,

                                District = company.District,
                                CityID = company.CityID,
                                CountryID = company.CountryID,
                                Phone = company.Phone,
                                Fax = company.Fax,

                                website = company.website,
                                email = company.email,
                                Mobile = company.Mobile,
                                ContactPerson = company.ContactPerson,

                                Registered = company.Registered,
                                Area = company.Area,
                                CityName = city.Name,
                                CountryName = contry.Name


                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

      
        public HttpResponseMessage SaveCompany(Comapany company)
        {
            try
            {
                if (company != null)
                {
                    if (company.CompanyID != 0)
                    {
                        Comapany b = db.Comapanies.Where(x => x.CompanyID == company.CompanyID).FirstOrDefault();
                        b.Name = string.IsNullOrEmpty(company.Name) ? "" : company.Name;
                        b.NameAR = company.NameAR;
                        b.District = company.District;
                        b.CityID = company.CityID;
                        b.Area = company.Area;
                        b.LandMark = company.LandMark;
                        b.CountryID = company.CountryID;
                        b.Phone = company.Phone;
                        b.Fax = company.Fax;
                        b.ContactPerson = company.ContactPerson;
                        b.Mobile = company.Mobile;
                        b.email = company.email;
                        b.website = company.website;
                        b.Registered = company.Registered;
                        //b.Modified_Date = DateTime.Now;
                        //b.Modified_By = Company.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }                    
                    else
                    {
                     
                        //Comapany b = new Comapany();
                        //b.Name = string.IsNullOrEmpty(company.Name) ? "" : company.Name;
                        //b.NameAR = company.NameAR;
                        //b.District = company.District;
                        //b.CityID = company.CityID;
                        //b.Area = company.Area;
                        //b.LandMark = company.LandMark;
                        //b.CountryID = company.CountryID;
                        //b.Phone = company.Phone;
                        //b.Fax = company.Fax;
                        //b.ContactPerson = company.ContactPerson;
                        //b.Mobile = company.Mobile;
                        //b.email = company.email;
                        //b.website = company.website;
                        //b.Registered = company.Registered;
                        db.Comapanies.Add(company);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, company);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
