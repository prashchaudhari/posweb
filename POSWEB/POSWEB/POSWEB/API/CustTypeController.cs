﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CustTypeController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetCustTypeList")]
        public HttpResponseMessage GetCustTypeList(long Id)
        {
            try
            {
                var list = (from brand in db.CustTypes
                            where brand.Deleted == false
                            select new
                            {
                                CustTypeID = brand.CustTypeID,
                                Description = brand.Description

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveCustType")]
        public HttpResponseMessage SaveCustType(CustType ct)
        {
            try
            {
                if (ct != null)
                {
                    if (ct.CustTypeID != 0 && (ct.Deleted_By == 0 || ct.Deleted_By == null))
                    {
                        CustType b = db.CustTypes.Where(x => x.CustTypeID == ct.CustTypeID).FirstOrDefault();
                        b.Description = string.IsNullOrEmpty(ct.Description) ? "" : ct.Description;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = ct.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (ct.CustTypeID != 0 && ct.Deleted_By != 0)
                    {
                        CustType b = db.CustTypes.Where(x => x.CustTypeID == ct.CustTypeID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = ct.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        CustType b = new CustType();
                        b.Description = string.IsNullOrEmpty(ct.Description) ? "" : ct.Description;
                        b.Created_Date = DateTime.Now;
                        b.Created_By = ct.Created_By;
                        b.Deleted = false;
                        db.CustTypes.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, ct);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
