﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class GroupController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetGroupList")]
        public HttpResponseMessage GetGroupList(long Id)
        {
            try
            {
                var list = (from grp in db.Groups
                            where grp.Deleted == false
                            select new
                            {
                                GroupID = grp.GroupID,
                                Code = grp.Code,
                                Description = grp.Description,
                                DescriptionAR = grp.DescriptionAR,
                                GUID = grp.GUID,
                                Note = grp.Note

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveGroup")]
        public HttpResponseMessage SaveGroup(Group grp)
        {
            try
            {
                if (grp != null)
                {
                    if (grp.GroupID != 0 && (grp.Deleted_By == 0 || grp.Deleted_By == null))
                    {
                        Group b = db.Groups.Where(x => x.GroupID == grp.GroupID).FirstOrDefault();
                        b.Code = string.IsNullOrEmpty(grp.Code) ? "" : grp.Code;
                        b.Description = grp.Description;
                        b.DescriptionAR = grp.DescriptionAR;
                        b.Note = grp.Note;
                        b.GUID = grp.GUID;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_by = grp.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (grp.GroupID != 0 && grp.Deleted_By != 0)
                    {
                        Group b = db.Groups.Where(x => x.GroupID == grp.GroupID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = grp.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Group b = new Group();
                        b.Code = string.IsNullOrEmpty(grp.Code) ? "" : grp.Code;
                        b.Description = grp.Description;
                        b.DescriptionAR = grp.DescriptionAR;
                        b.GUID = grp.GUID;
                        b.Note = grp.Note;
                        b.Created_Date = DateTime.Now;
                        b.Created_By = grp.Created_By;
                        b.Deleted = false;
                        db.Groups.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, grp);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
