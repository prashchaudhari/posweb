﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class UOMController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        public HttpResponseMessage GetUOMList(long Id)
        {
            try
            {
                var list = (from uom in db.UOMs
                            where uom.Deleted == false
                            select new
                            {
                                UOMID = uom.UOMID,
                                Description = uom.Description
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveUOM")]
        public HttpResponseMessage SaveUOM(UOM subcat)
        {
            try
            {
                if (subcat != null)
                {
                    if (subcat.UOMID != 0 && (subcat.Deleted_By == 0 || subcat.Deleted_By == null))
                    {
                        UOM b = db.UOMs.Where(x => x.UOMID == subcat.UOMID).FirstOrDefault();
                        b.Description = subcat.Description;
                       
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = subcat.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (subcat.UOMID != 0 && subcat.Deleted_By != 0)
                    {
                        UOM b = db.UOMs.Where(x => x.UOMID == subcat.UOMID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = subcat.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        UOM b = new UOM();
                        b.Description = subcat.Description;                       
                        b.Created_Date = DateTime.Now;
                        b.Created_by = subcat.Created_by;
                        b.Deleted = false;
                        db.UOMs.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, subcat);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
