﻿app.controller("CountryController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.CountryList = function () {

        $http({
            url: '../api/Country/GetCountryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.Country = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Country.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Country, params.orderBy()) : $scope.Country;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.CountryList();


    $scope.AddCountry = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelCountry = function () {
        $scope.panel = true;
    }

    $scope.SubmitCountry = function () {

        if ($scope.Validation() != false) {
            var Countrymaster = {
                "Name": $scope.Name,
                "CountryCode": $scope.CountryCode,
                "NameAR": $scope.NameAR,
                "Created_by": userid
            };

            $http({
                url: '../api/Country/SaveCountry',
                data: Countrymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CountryList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditCountry = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.CountryID;
        $scope.Name = $scope.Name;
        $scope.CountryCode = $scope.CountryCode;
        $scope.NameAR = $scope.NameAR;

    }

    $scope.UpdateCountry = function () {
        if ($scope.Validation() != false) {
            var Countrymaster = {
                "CountryID": bid,
                "Name": $scope.Name,
                "CountryCode": $scope.CountryCode,
                "NameAR": $scope.NameAR,

                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Country/SaveCountry',
                data: Countrymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Country) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CountryList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteCountry = function (id) {

        var Countrymaster = {
            "CountryID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Country/SaveCountry',
            data: Countrymaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.CountryList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.CountryCode == "" || $scope.CountryCode == undefined || $scope.CountryCode == null) {
            $scope.errormessage = "Enter Country Code";
            return false;
        }
        if ($scope.Name == "" || $scope.Name == undefined || $scope.Name == null) {
            $scope.errormessage = "Enter Country Name";
            return false;
        }
        if ($scope.NameAR == "" || $scope.NameAR == undefined || $scope.NameAR == null) {
            $scope.errormessage = "Enter Country NameAR";
            return false;
        }

        return true;
    }

    $scope.ClearText = function () {
        $scope.CountryCode = "";
        $scope.Name = "";
        $scope.NameAR = "";
    }
});