﻿app.controller("CurrencyController", function ($scope, $window, $http, $timeout,$log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.CurrencyList = function () {

        $http({
            url: '../api/Currency/GetCurrencyList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.branch = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.branch.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.branch, params.orderBy()) : $scope.branch;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
             })
            .error(function (error) {
                   // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;
       
    }
    $scope.CurrencyList();    
   
    
    $scope.AddCurrency = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();      
    }

    $scope.CancelCurrency = function () {
        $scope.panel = true;
    }

    $scope.SubmitCurrency = function () {    

        if ($scope.Validation() != false) {
            var master = {
                "Description": $scope.Description,
                "ISO": $scope.ISO,
                "Created_by": userid
            };

            $http({
                url: '../api/Currency/SaveCurrency',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CurrencyList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditCurrency = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();
       
        bid = rowdata.CurrencyID,
            $scope.ISO = rowdata.ISO,
        $scope.Description = rowdata.Description
            
    }

    $scope.UpdateCurrency = function () {        
        if ($scope.Validation() != false) {
            var master = {
                "CurrencyID": bid,
                "Description": $scope.Description,                
                "ISO": $scope.ISO,
                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Currency/SaveCurrency',
                data: master,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (brand) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CurrencyList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteCurrency = function (id) {
        
        var master = {
            "CurrencyID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Currency/SaveCurrency',
            data: master,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (brand) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.CurrencyList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.Description == "" || $scope.Description == undefined || $scope.Description == null)
        {
            $scope.errormessage = "Enter Currency Description";
            return false;
        }
        if ($scope.ISO == "" || $scope.ISO == undefined || $scope.ISO == null) {
            $scope.errormessage = "Enter Currency ISO";
            return false;
        }
        
        return true;
    }

    $scope.ClearText = function ()
    {
        $scope.Description = "";
        $scope.ISO = "";
       
    }
});