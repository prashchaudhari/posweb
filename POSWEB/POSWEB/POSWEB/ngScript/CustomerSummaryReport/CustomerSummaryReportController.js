﻿app.controller("CustomerSummaryReportController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var currentyear;
    var cmonth, cyear;
    //$('#from').pickadate({
    //    format: 'dd-mmm-yyyy',
    //    formatSubmit: 'yyyy/mm/dd',
    //    autoclose: true

    //});
    //$('#to').pickadate({
    //    format: 'dd-mmm-yyyy',
    //    formatSubmit: 'yyyy/mm/dd',
    //    dateMin: $scope.fromdate,
    //    autoclose: true

    //});    
    $('.pickadate').pickadate({
        format: 'dd-mmm-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        //min : [2017,12,01],
        autoclose: true
    });

    var currdate = new Date();
    var day = currdate.getDate();
    var month = currdate.getMonth() + 1;
    var year = currdate.getFullYear();
    currdate = year + '/' + month + '/' + day;

    $scope.CustomerSummary = function () {
        var fcdate, tcdate;
        if ($scope.fromdate == "undefined" || $scope.fromdate == null) {
            fcdate = currdate;
        }
        else {
            fcdate = $scope.fromdate;
        }
        if ($scope.todate == "undefined" || $scope.todate == null) {
            tcdate = currdate;
        }
        else {
            tcdate = $scope.todate;
        }
        $http({
            url: '../api/CustomerSummaryReport/GetCustomerSummary?fdate=' + fcdate + '&tdate=' + tcdate,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
                //alert(JSON.stringify(response.datalist));

                $scope.CustomerDetails = response.datalist;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.CustomerDetails.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.CustomerDetails, params.orderBy()) : $scope.CustomerDetails;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
                
                var Combined = new Array();
                // Chart settings
                // Data
                Combined[0] = ['ShortName', 'TotalPay'];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].ShortName, response.linegraphdata[i].TotalPay];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options_pie = {
                    fontName: 'Roboto',
                    height: 300,
                    width: 500,
                    chartArea: {
                        left: 50,
                        width: '90%',
                        height: '90%'
                    }
                };

                // Instantiate and draw our chart, passing in some options.
                var pie = new google.visualization.PieChart($('#google-pie')[0]);
                pie.draw(data, options_pie);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    }
    $scope.CustomerSummary();
});