﻿app.controller("RoleController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.RoleList = function () {

        $http({
            url: '../api/Role/GetRoleList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.Role = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.Role.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.Role, params.orderBy()) : $scope.Role;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.RoleList();

    $scope.ObjectList = function () {

        $http({
            url: '../api/Object/GetObjectList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.ObjectList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    

    $scope.AddRole = function () {
        $scope.ObjectList();
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelRole = function () {
        $scope.panel = true;
    }

    $scope.SubmitRole = function () {

        if ($scope.Validation() != false) {
            var Rolemaster = {
                "RoleName": $scope.RoleName,
                "ObjectID": $scope.ObjectID,
                "FullAccess": $scope.FullAccess,
                "DisplayAccess": $scope.DisplayAccess,
                "NoAccess": $scope.NoAccess,
                "Created_by": userid
            };

            $http({
                url: '../api/Role/SaveRole',
                data: Rolemaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.RoleList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditRole = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.RoleID;
        $scope.RoleName = rowdata.RoleName;
        $scope.ObjectID = rowdata.ObjectID;
        $scope.FullAccess = rowdata.FullAccess;
        $scope.DisplayAccess = rowdata.DisplayAccess;
        $scope.NoAccess = rowdata.NoAccess;
    }

    $scope.UpdateRole = function () {
        if ($scope.Validation() != false) {
            var Rolemaster = {
                "RoleID": bid,
                "RoleName": $scope.RoleName,
                "ObjectID": $scope.ObjectID,
                "FullAccess": $scope.FullAccess,
                "DisplayAccess": $scope.DisplayAccess,
                "NoAccess": $scope.NoAccess,
                "Created_by": userid
            };

            $http({
                url: '../api/Role/SaveRole',
                data: Rolemaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (Role) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.RoleList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteRole = function (id) {

        var Rolemaster = {
            "RoleID": id
        };
        $http({
            url: '../api/Role/SaveRole',
            data: Rolemaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.RoleList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.RoleName == "" || $scope.RoleName == undefined || $scope.RoleName == null) {
            $scope.errormessage = "Enter RoleName";
            return false;
        }
        if ($scope.ObjectID == "" || $scope.ObjectID == undefined || $scope.ObjectID == null) {
            $scope.errormessage = "Select Object";
            return false;
        }        

        return true;
    }

    $scope.ClearText = function () {
        $scope.RoleName = "";
        $scope.ObjectID = "";
        $scope.FullAccess = "";
        $scope.DisplayAccess = "";
        $scope.NoAccess = "";
        $scope.errormessage = "";
    }
});