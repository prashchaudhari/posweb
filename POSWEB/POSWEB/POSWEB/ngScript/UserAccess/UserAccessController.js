﻿app.controller("UserAccessController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.UserAccessList = function () {
        $http({
            url: '../api/UserAccess/GetUserAccessList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
               
                $scope.branch = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.branch.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.branch, params.orderBy()) : $scope.branch;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.UserAccessList();


    $scope.AddUserAccess = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelUserAccess = function () {
        $scope.panel = true;
    }

    $scope.SubmitUserAccess = function () {

        if ($scope.Validation() != false) {
            var UserAccessmaster = {
                "UserID": $scope.UserID,
                "BranchID": $scope.BranchID,
                "AllowAccess": $scope.AllowAccess,
                "Created_By": userid
                
            };

            $http({
                url: '../api/UserAccess/SaveUserAccess',
                data: UserAccessmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.UserAccessList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditUserAccess = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.AutoID,
        $scope.UserID = rowdata.UserID,
        $scope.BranchID = rowdata.BranchID,
            $scope.AllowAccess = rowdata.AllowAccess,
            $scope.AutoID=ro.AutoID
    }

    $scope.UpdateUserAccess = function () {
        if ($scope.Validation() != false) {
            var UserAccessmaster = {
                "UserID": $scope.UserID,
                "BranchID": $scope.BranchID,
                "AllowAccess": $scope.AllowAccess,
                "Created_By": userid,
                "AutoID":bid
            };

            $http({
                url: '../api/UserAccess/SaveUserAccess',
                data: UserAccessmaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (UserAccess) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.UserAccessList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteUserAccess = function (AutoID) {

        var UserAccessmaster = {
            "AutoID": AutoID,
            "Deleted_By": userid
        };
        $http({
            url: '../api/UserAccess/SaveUserAccess',
            data: UserAccessmaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (UserAccess) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.UserAccessList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.UserID == "" || $scope.UserID == undefined || $scope.UserID == null) {
            $scope.errormessage = "Select User Name ";
            return false;
        }
        if ($scope.BranchID == "" || $scope.BranchID == undefined || $scope.BranchID == null) {
            $scope.errormessage = "Select Branch Name ";
            return false;
        }
        
        return true;
    }
    $scope.GetBranchList = function () {

        $http({
            url: '../api/Branch/GetBranchList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.BranchList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetBranchList();
    $scope.GetUserList = function () {

        $http({
            url: '../api/User/GetUserList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data));  
                $scope.UserList = data;
            })
            .error(function (error) {
                // alert("Error in Get City List");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
    }
    $scope.GetUserList();
    $scope.ClearText = function () {
        $scope.UserID = "";
        $scope.BranchID = "";
        $scope.AllowAccess = false;


    }
});