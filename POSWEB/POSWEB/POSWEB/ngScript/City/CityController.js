﻿app.controller("CityController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
   // alert(userid);
    var bid;
    $scope.success = true; $scope.error = true; $scope.listerror = true; $scope.delete = true;
    $scope.CityList = function () {

        $http({
            url: '../api/Citys/GetCityList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.City = data;
                $scope.tableParams = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                        total: $scope.City.length,
                        getData: function ($defer, params) {
                            $scope.data = params.sorting() ? $filter('orderBy')($scope.City, params.orderBy()) : $scope.City;
                            $scope.data = params.filter() ? $filter('filter')($scope.data, params.filter()) : $scope.data;
                            $scope.data = $scope.data.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }
    $scope.CityList();


    $scope.AddCity = function () {
        $scope.panel = false;
        $scope.ClearText();
        $('#btnsave').show();
        $('#btnupdate').hide();
    }

    $scope.CancelCity = function () {
        $scope.panel = true;
    }

    $scope.SubmitCity = function () {

        if ($scope.Validation() != false) {
            var Citymaster = {
                "Name": $scope.Name,
                "CityCode": $scope.CityCode,
                "NameAR": $scope.NameAR,
                "Created_by": userid
            };

            $http({
                url: '../api/Citys/SaveCity',
                data: Citymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (state) {
                    //alert("Branch Data Save Successful");               
                    // $("#successdiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CityList();

                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(1000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)
                })

            $scope.panel = true;
        }
    }

    $scope.EditCity = function (rowdata) {
        //alert(JSON.stringify(rowdata));
        $scope.errormessage = "";
        $scope.panel = false;
        $('#btnsave').hide();
        $('#btnupdate').show();

        bid = rowdata.CityID;
        $scope.Name = $scope.Name;
        $scope.CityCode = $scope.CityCode;
        $scope.NameAR = $scope.NameAR;

    }

    $scope.UpdateCity = function () {
        if ($scope.Validation() != false) {
            var Citymaster = {
                "CityID": bid,
                "Name": $scope.Name,
                "CityCode": $scope.CityCode,
                "NameAR": $scope.NameAR,

                "Deleted": false,
                "Created_by": userid
            };

            $http({
                url: '../api/Citys/SaveCity',
                data: Citymaster,
                datatype: 'json',
                method: 'POST',
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json"
                }
            })
                .success(function (City) {
                    // alert("Branch Data update Successful");                
                    //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.success = false;
                    $timeout(function () {
                        $scope.success = true;
                    }, 3000)
                    $scope.CityList();
                })
                .error(function () {
                    //alert("Error in Save Branch Data");
                    //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                    $scope.error = false;
                    $timeout(function () {
                        $scope.error = true;
                    }, 3000)

                })

            $scope.panel = true;
        }
    }

    $scope.DeleteCity = function (id) {

        var Citymaster = {
            "CityID": id,
            "Deleted_By": userid
        };
        $http({
            url: '../api/Citys/SaveCity',
            data: Citymaster,
            datatype: 'json',
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert("Branch Data update Successful");                
                //$("#successdiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.delete = false;
                $timeout(function () {
                    $scope.delete = true;
                }, 3000)
                $scope.CityList();
            })
            .error(function () {
                //alert("Error in Save Branch Data");
                //$("#errordiv").fadeIn("slow").delay(10000).fadeOut("slow");
                $scope.error = false;
                $timeout(function () {
                    $scope.error = true;
                }, 3000)

            })
    }

    $scope.Validation = function () {
        if ($scope.CityCode == "" || $scope.CityCode == undefined || $scope.CityCode == null) {
            $scope.errormessage = "Enter City CityCode";
            return false;
        }
        if ($scope.Name == "" || $scope.Name == undefined || $scope.Name == null) {
            $scope.errormessage = "Enter City Name";
            return false;
        }
        if ($scope.NameAR == "" || $scope.NameAR == undefined || $scope.NameAR == null) {
            $scope.errormessage = "Enter City NameAR";
            return false;
        }

        return true;
    }

    $scope.ClearText = function () {
        $scope.CityCode = "";
        $scope.Name = "";
        $scope.NameAR = "";
    }
});