﻿app.controller("SaleAnalysisReportController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var currentyear;
    var cmonth, cyear;
    
    $('.pickadate').pickadate({
        format: 'dd-mmm-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        //min : [2017,12,01],
        autoclose: true
    });

    var currdate = new Date();
    var day = currdate.getDate();
    var month = currdate.getMonth() + 1;
    var year = currdate.getFullYear();
    currdate = year + '/' + month + '/' + day;

    $scope.SaleAnalysis = function () {
        var fcdate, tcdate;
        if ($scope.fromdate == "undefined" || $scope.fromdate == null) {
            fcdate = currdate;
        }
        else {
            fcdate = $scope.fromdate;
        }
        if ($scope.todate == "undefined" || $scope.todate == null) {
            tcdate = currdate;
        }
        else {
            tcdate = $scope.todate;
        }
        $http({
            url: '../api/SaleAnalysisReport/GetSale?fdate=' + fcdate + '&tdate=' + tcdate,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
                //alert(JSON.stringify(response.datalist));                
                var Combined = new Array();
                // Chart settings
                // Data
                Combined[0] = ['Brand', 'TotalSales', { role: 'style' }];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].Description, response.linegraphdata[i].NetAmount, response.linegraphdata[i].ColorCode];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options_pie = {
                    fontName: 'Roboto',
                    height: 300,
                    width: 500,
                    chartArea: {
                        left: 50,
                        width: '90%',
                        height: '90%'
                    },
                    colors: ['red', 'green'],
                };
                    
                // Instantiate and draw our chart, passing in some options.
                var pie = new google.visualization.PieChart($('#google-pie')[0]);
                pie.draw(data, options_pie);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    }
    $scope.SaleAnalysis();
});