﻿
app.controller("SalesReportController", function ($scope, $window, $http, $timeout, $log, $filter, ngTableParams) {

    var username = sessionStorage.getItem("UserName");
    var userid = sessionStorage.getItem("UserID");
    //alert(userid);
    var currentyear;
    var cmonth, cyear;
    $('#from').pickadate({
        format: 'dd-mmm-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        autoclose: true

    });
    $('#to').pickadate({
        format: 'dd-mmm-yyyy',
        formatSubmit: 'yyyy/mm/dd',
        dateMin: $scope.fromdate,
        autoclose: true

    });
    $('.pickadate').pickadate({
        
        format: 'mm-yyyy', 
        formatSubmit: 'mm-yyyy', 
        autoclose: true,
        selectMonths: true,
        selectYears: true
        
    });
    
    var currdate = new Date();
    var day = currdate.getDate();
    var month = currdate.getMonth() + 1;
    var year = currdate.getFullYear();
    currdate = year + '/' + month + '/' + day;    

   
    $scope.SaleSummary = function () {   
        
        if ($scope.curryear == "undefined" || $scope.curryear == 0 || $scope.curryear == null) {
            currentyear = year;
        }
        else {
            currentyear = $scope.curryear;
        }
        var fromdate = currentyear + '/01/01';
        var todate = currentyear + '/12/31';
        $http({
            url: '../api/SalesReport/GetSalesSummaryReport?fdate=' + fromdate + '&tdate=' + todate,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
                //alert(JSON.stringify(response.linegraphdata));
                var json = [];
                var janpaid = 0; var febpaid = 0; var marpaid = 0; var aprpaid = 0; var maypaid = 0; var junpaid = 0;
                var julpaid = 0; var augpaid = 0; var seppaid = 0; var octpaid = 0; var novpaid = 0; var decpaid = 0;

                var janunpaid = 0; var febunpaid = 0; var marunpaid = 0; var aprunpaid = 0; var mayunpaid = 0; var jununpaid = 0;
                var julunpaid = 0; var augunpaid = 0; var sepunpaid = 0; var octunpaid = 0; var novunpaid = 0; var decunpaid = 0;

                $scope.tabxablesales = response.tabxablesales; $scope.nontaxablesales = response.nontaxablesales;
                $scope.totalproductsales = response.totalproductsales; $scope.itemdiscount = response.itemdiscount;
                $scope.netsales = response.netsales;
                $scope.accepted = response.accepted; $scope.hold = response.hold; $scope.return = response.returns;
                $scope.acceptedamt = response.acceptedamt; $scope.holdamt = response.holdamt; $scope.returnamt = response.returnsamt;
                json = response.linegraphdata;
                json.forEach(function (month) {
                    if (month.MonthName.startsWith("January")) {
                        janpaid = month.paid;
                        janunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("February")) {
                        febpaid = month.paid;
                        febunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("March")) {
                        marpaid = month.paid;
                        marunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("April")){
                        aprpaid = month.paid;
                        aprunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("May")) {
                        maypaid = month.paid;
                        mayunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("June")) {
                        junpaid = month.paid;
                        jununpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("July")) {
                        julpaid = month.paid;
                        julunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("August")) {
                        augpaid = month.paid;
                        augunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("September")) {
                        seppaid = month.paid;
                        sepunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("October")) {
                        octpaid = month.paid;
                        octunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("November")) {
                        novpaid = month.paid;
                        novunpaid = month.unpaid;
                    }
                    if (month.MonthName.startsWith("Dec")) {
                        decpaid = month.paid;
                        decunpaid = month.unpaid;
                    }
                });
                // Chart settings
                // Data
                var data = google.visualization.arrayToDataTable([
                    ['Month', 'Paid', 'Unpaid'],
                    ['Jan', janpaid, janunpaid],
                    ['Feb', febpaid, febunpaid],
                    ['Mar', marpaid, marunpaid],
                    ['Apr', aprpaid, aprunpaid],
                    ['May', maypaid, mayunpaid],
                    ['Jun', junpaid, jununpaid],
                    ['Jul', julpaid, julunpaid],
                    ['Aug', augpaid, augunpaid],
                    ['Sep', seppaid, sepunpaid],
                    ['Oct', octpaid, octunpaid],
                    ['Nov', novpaid, novunpaid],
                    ['Dec', decpaid, decunpaid],
                ]);

                // Options
                var options = {
                    fontName: 'Roboto',
                    height: 400,
                    curveType: 'function',
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 250
                    },
                    pointSize: 4,
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Sales',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var line_chart = new google.visualization.LineChart($('#google-line')[0]);
                line_chart.draw(data, options);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    }
    $scope.SaleSummary();


    $scope.DayWise = function () {   
        var cmonth, cyear;
        if ($scope.monthyear == "undefined" || $scope.monthyear == 0 || $scope.monthyear == null) {
            cmonth = month; cyear = year;
        }
        else {
            var monthy = $scope.monthyear.split('-');
            cmonth = monthy[0]; cyear = monthy[1];
        }        
        $http({
            url: '../api/SalesReport/GetDayWiseReport?month=' + cmonth + '&year=' + cyear,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
               // alert(JSON.stringify(response.linegraphdata));               
                var Combined = new Array();
                $scope.dtabxablesales = response.tabxablesales; $scope.dnontaxablesales = response.nontaxablesales;
                $scope.dtotalproductsales = response.totalproductsales; $scope.ditemdiscount = response.itemdiscount;
                $scope.dnetsales = response.netsales;
                $scope.daccepted = response.accepted; $scope.dhold = response.hold; $scope.dreturn = response.returns;
                $scope.dacceptedamt = response.acceptedamt; $scope.dholdamt = response.holdamt; $scope.dreturnamt = response.returnsamt;
                              
                // Chart settings
                // Data
                Combined[0] = ['Dates', 'Paid', 'Unpaid'];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].Days, response.linegraphdata[i].paid, response.linegraphdata[i].unpaid];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options = {
                    fontName: 'Roboto',
                    height: 400,
                    curveType: 'function',
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 250
                    },
                    pointSize: 4,
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Sales',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            //color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var line_chart = new google.visualization.LineChart($('#google-line_day')[0]);
                line_chart.draw(data, options);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            }); 
    }  

    $scope.AllProduct = function () {
        $scope.BrandList();
        $scope.CategoryList();
        $scope.GroupList();
        $scope.GetSubCategoryList();
        $scope.ShowAllProduct();
    }

    $scope.ShowAllProduct = function () {
        var cmonth, cyear;
        if ($scope.crmonthyear == "undefined" || $scope.crmonthyear == 0 || $scope.crmonthyear == null) {
            cmonth = month; cyear = year;
        }
        else {
            var monthy = $scope.crmonthyear.split('-');
            cmonth = monthy[0]; cyear = monthy[1];
        } 
        var data = {
            month:cmonth,
            year:cyear,
            BrandID : $scope.BrandID,
            CategoryID : $scope.CategoryID,
            SubCategoryID : $scope.SubCategoryID,
            GroupID : $scope.GroupID
        }
        $http({
            url: '../api/SalesReport/GetAllProductReport',
            datatype: 'json',
            data: data,
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
                // alert(JSON.stringify(response.linegraphdata));               
                var Combined = new Array();
                $scope.alltabxablesales = response.tabxablesales; $scope.allnontaxablesales = response.nontaxablesales;
                $scope.alltotalproductsales = response.totalproductsales; $scope.allitemdiscount = response.itemdiscount;
                $scope.allnetsales = response.netsales;               
                // Chart settings
                // Data
                Combined[0] = ['CODE', 'Salecount', 'Return','Purchase'];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].CODE, response.linegraphdata[i].Salecount, response.linegraphdata[i].Rutruncont, response.linegraphdata[i].Purchasecount];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options = {
                    fontName: 'Roboto',
                    height: 400,
                    curveType: 'function',
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 250
                    },
                    pointSize: 4,
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Count',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var line_chart = new google.visualization.ColumnChart($('#google-line_allproduct')[0]);
                line_chart.draw(data, options);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    } 

    $scope.OrderHistory = function () {
        var cmonth, cyear;
        if ($scope.historymonth == "undefined" || $scope.historymonth == 0 || $scope.historymonth == null) {
            cmonth = month; cyear = year;
        }
        else {
            var monthy = $scope.historymonth.split('-');
            cmonth = monthy[0]; cyear = monthy[1];
        }
        $http({
            url: '../api/SalesReport/GetOrderHistoryReport?flag=0&month=' + cmonth + '&year=' + cyear,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
                 //alert(JSON.stringify(response.linegraphdata));               
                var Combined = new Array();
                $scope.ohtabxablesales = response.tabxablesales; $scope.ohnontaxablesales = response.nontaxablesales;
                $scope.ohtotalproductsales = response.totalproductsales; $scope.ohitemdiscount = response.itemdiscount;
                $scope.ohnetsales = response.netsales;
                $scope.ohaccepted = response.accepted; $scope.ohhold = response.hold; /*$scope.dreturn = response.returns;*/
                $scope.ohacceptedamt = response.acceptedamt; $scope.ohholdamt = response.holdamt; /*$scope.dreturnamt = response.returnsamt;*/

                // Chart settings
                // Data
                Combined[0] = ['Days', 'Accepted', 'Hold'];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].Days, response.linegraphdata[i].Accepted, response.linegraphdata[i].Hold];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options = {
                    fontName: 'Roboto',
                    height: 400,
                    curveType: 'function',
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 250
                    },
                    pointSize: 4,
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Sales',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var line_chart = new google.visualization.LineChart($('#google-line_orderhistory')[0]);
                line_chart.draw(data, options);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    }  

    $scope.PaymentSummary = function () { 
        var fcdate,tcdate;
        if ($scope.fromdate == "undefined" || $scope.fromdate == 0 || $scope.fromdate == null) {
            fcdate = currdate;
        }
        else {
            fcdate = $scope.fromdate;
        }
        if ($scope.todate == "undefined" || $scope.todate == 0 || $scope.todate == null) {
            tcdate = currdate;
        }
        else {
            tcdate = $scope.todate;
        }
        $http({
            url: '../api/SalesReport/GetPayementSummary?flag=0&fromdate=' + fcdate + '&todate=' + tcdate,
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (response) {
               // alert(JSON.stringify(response.linegraphdata));               
                var Combined = new Array();
                //$scope.ohtabxablesales = response.tabxablesales; $scope.ohnontaxablesales = response.nontaxablesales;
                //$scope.ohtotalproductsales = response.totalproductsales; $scope.ohitemdiscount = response.itemdiscount;
                //$scope.ohnetsales = response.netsales;
                //$scope.ohaccepted = response.accepted; $scope.ohhold = response.hold; 
                //$scope.ohacceptedamt = response.acceptedamt; $scope.ohholdamt = response.holdamt; 

                // Chart settings
                // Data
                Combined[0] = ['TenderName', 'TotalAmount'];
                for (var i = 0; i < response.linegraphdata.length; i++) {
                    Combined[i + 1] = [response.linegraphdata[i].TenderName, response.linegraphdata[i].TotalAmount];
                }
                //second parameter is false because first row is headers, not data.
                var data = google.visualization.arrayToDataTable(Combined, false);

                // Options
                var options = {
                    fontName: 'Roboto',
                    height: 400,
                    curveType: 'function',
                    fontSize: 12,
                    chartArea: {
                        left: '5%',
                        width: '90%',
                        height: 250
                    },
                    pointSize: 4,
                    tooltip: {
                        textStyle: {
                            fontName: 'Roboto',
                            fontSize: 13
                        }
                    },
                    vAxis: {
                        title: 'Sales',
                        titleTextStyle: {
                            fontSize: 13,
                            italic: false
                        },
                        gridlines: {
                            color: '#e5e5e5',
                            count: 10
                        },
                        minValue: 0
                    },
                    legend: {
                        position: 'top',
                        alignment: 'center',
                        textStyle: {
                            fontSize: 12
                        }
                    }
                };

                // Draw chart
                var line_chart = new google.visualization.ColumnChart($('#google-line_paymentsummary')[0]);
                line_chart.draw(data, options);

            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");               
            });
    }  

    $scope.BrandList = function () {

        $http({
            url: '../api/Brand/GetBrandList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.BrandList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
       

    }    
    $scope.CategoryList = function () {

        $http({
            url: '../api/Category/GetCategoryList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                //alert(JSON.stringify(data)); 
                $scope.CategoryList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        $scope.panel = true;

    }    
    $scope.GroupList = function () {

        $http({
            url: '../api/Group/GetGroupList?Id=0',
            datatype: 'json',
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.GroupList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
       

    } 
    $scope.GetSubCategoryList = function () {
        var categoryid = $scope.CategoryID;
        if (categoryid == "undefined" || categoryid == null || categoryid=="") {
            categoryid = 0;
        }
        
        $http({
            url: '../api/SubCategory/GetSubCategory?Cat=' + categoryid+'&flag=0',
            datatype: 'json',
            method: "GET",  
            headers: {
                "Content-Type": "application/json",
                "accept": "application/json"
            }
        })
            .success(function (data) {
                // alert(JSON.stringify(data)); 
                $scope.SubCategoryList = data;                
            })
            .error(function (error) {
                // alert("Error in Get Branch Detail");
                $scope.listerror = false;
                $timeout(function () {
                    $scope.listerror = true;
                }, 3000)
            });
        

    }

   
});