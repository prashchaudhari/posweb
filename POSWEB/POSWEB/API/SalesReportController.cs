﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class SalesReportController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]        
        public HttpResponseMessage GetSalesSummaryReport(DateTime fdate, DateTime tdate)
        {
            try
            {
                SaleSummary ss = new SaleSummary();
                var list = db.Get_LineGraphMonthWiseData(fdate, tdate).ToList();
                ss.linegraphdata = list.ToArray();

                var sales = (from t in db.Transactions
                            where t.TransactionDate>=fdate && t.TransactionDate <=tdate
                            select t).ToList();
                var taxsales = sales.Where(x => x.TaxAmount != 0).Sum(x => x.NetAmount);
                var nontaxsales = sales.Where(x=>x.TaxAmount == 0).Sum(x => x.NetAmount);
                var discount = sales.Sum(x => x.FixedDiscount);
                var hold = sales.Where(x => x.TransType == "SALE" && x.flag == false).Count();
                var holdamt = sales.Where(x => x.TransType == "SALE" && x.flag == false).Sum(x=>x.NetAmount);
                var accepted = sales.Where(x => x.TransType == "SALE" && x.flag == true).Count();
                var acceptedamt = sales.Where(x => x.TransType == "SALE" && x.flag == true).Sum(x => x.NetAmount);
                var returs = sales.Where(x => x.TransType == "RETURN").Count();
                var retursamt = sales.Where(x => x.TransType == "RETURN").Sum(x => x.NetAmount);
                retursamt = retursamt * -1;

                ss.tabxablesales = taxsales; ss.nontaxablesales = nontaxsales;
                ss.totalproductsales = taxsales + nontaxsales; ss.itemdiscount = discount;
                ss.netsales = ss.totalproductsales - ss.itemdiscount;
                ss.hold = hold; ss.accepted = accepted; ss.returns = returs;
                ss.holdamt = holdamt; ss.acceptedamt = acceptedamt; ss.returnsamt = retursamt;

                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpGet]        
        public HttpResponseMessage GetDayWiseReport(int month, int year)
        {
            try
            {
                
                DateTime? fdate = Convert.ToDateTime("01-" + month + "-" + year);
               // DateTime? tdate = Convert.ToDateTime("31-" + month + "-" + year);
                SaleSummary ss = new SaleSummary();
                var list = db.Get_LineGraphDayWiseData(month, year).ToList();
                ss.linegraphdata = list.ToArray();

                var sales = (from t in db.Transactions
                             where t.TransactionDate.Value.Month == fdate.Value.Month && t.TransactionDate.Value.Year == fdate.Value.Year
                             select t).ToList();
                var taxsales = sales.Where(x => x.TaxAmount != 0).Sum(x => x.NetAmount);
                var nontaxsales = sales.Where(x => x.TaxAmount == 0).Sum(x => x.NetAmount);
                var discount = sales.Sum(x => x.FixedDiscount);
                var hold = sales.Where(x => x.TransType == "SALE" && x.flag == false).Count();
                var holdamt = sales.Where(x => x.TransType == "SALE" && x.flag == false).Sum(x => x.NetAmount);
                var accepted = sales.Where(x => x.TransType == "SALE" && x.flag == true).Count();
                var acceptedamt = sales.Where(x => x.TransType == "SALE" && x.flag == true).Sum(x => x.NetAmount);
                var returs = sales.Where(x => x.TransType == "RETURN").Count();
                var retursamt = sales.Where(x => x.TransType == "RETURN").Sum(x => x.NetAmount);
                retursamt = retursamt * -1;

                ss.tabxablesales = taxsales; ss.nontaxablesales = nontaxsales;
                ss.totalproductsales = taxsales + nontaxsales; ss.itemdiscount = discount;
                ss.netsales = ss.totalproductsales - ss.itemdiscount;
                ss.hold = hold; ss.accepted = accepted; ss.returns = returs;
                ss.holdamt = holdamt; ss.acceptedamt = acceptedamt; ss.returnsamt = retursamt;

                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAllProdudctReport(AllProduct ap)
        {
            try
            {
                long? BrandId, CategoryId, SubCategoryId, GroupId;
                if(ap.BrandID == 0) { BrandId = null; } else { BrandId = ap.BrandID; }
                if (ap.CategoryID == 0) { CategoryId = null; } else { CategoryId = ap.CategoryID; }
                if (ap.SubCategoryID == 0) { SubCategoryId = null; } else { SubCategoryId = ap.SubCategoryID; }
                if (ap.GroupID == 0) { GroupId = null; } else { GroupId = ap.GroupID; }
                DateTime? fdate = Convert.ToDateTime("01-" + ap.month + "-" + ap.year);                
                SaleSummary ss = new SaleSummary();
                var list = db.Get_LineGraphAllProductData(ap.month, ap.year, BrandId, CategoryId, SubCategoryId, GroupId).ToList();
                ss.linegraphdata = list.ToArray();

                var query = (from p in db. Products
                             join c in db.Conversi0n on p.ProductID equals c.ProductID
                             join s in db.Stocks on c.AutoID equals s.ProductID
                             join t in db.Transactions on s.TransactionID equals t.TransactionID.ToString()
                             join b in db.Brands on p.BrandID equals b.BrandID
                             join cat in db.Categories on p.CategoryID equals cat.CategoryID
                             join sc in db.SubCategories on p.SubCategoryID equals sc.SubCategoryID
                             join g in db.Groups on p.GroupID equals g.GroupID
                             
                             where (
                             (t.TransactionDate.Value.Month == fdate.Value.Month) && (t.TransactionDate.Value.Year == fdate.Value.Year)
                             && (ap.BrandID != 0 ? p.BrandID == ap.BrandID : true == true)
                              && (ap.CategoryID != 0 ? p.CategoryID == ap.CategoryID : true == true)
                              && (ap.SubCategoryID != 0 ? p.SubCategoryID == ap.SubCategoryID : true == true)
                               && (ap.GroupID != 0 ? p.GroupID == ap.GroupID : true == true)
                               )
                             select t).ToList();

                var taxsales = query.Where(x => x.TaxAmount != 0).Sum(x => x.NetAmount);
                var nontaxsales = query.Where(x => x.TaxAmount == 0).Sum(x => x.NetAmount);
                var discount = query.Sum(x => x.FixedDiscount);

                ss.tabxablesales = taxsales; ss.nontaxablesales = nontaxsales;
                ss.totalproductsales = taxsales + nontaxsales; ss.itemdiscount = discount;
                ss.netsales = ss.totalproductsales - ss.itemdiscount;

                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpGet]
        public HttpResponseMessage GetOrderHistoryReport(string flag,int month, int year)
        {
            try
            {

                DateTime? fdate = Convert.ToDateTime("01-" + month + "-" + year);
                // DateTime? tdate = Convert.ToDateTime("31-" + month + "-" + year);
                SaleSummary ss = new SaleSummary();
                var list = db.Get_LineGraphOrderHistoryData(month, year).ToList();
                ss.linegraphdata = list.ToArray();

                var order = (from t in db.SOTransactions
                             where t.TransactionDate.Month == fdate.Value.Month && t.TransactionDate.Year == fdate.Value.Year
                             select t).ToList();
                var taxsales = order.Where(x => x.TaxAmount != 0).Sum(x => x.NetAmount);
                var nontaxsales = order.Where(x => x.TaxAmount == 0).Sum(x => x.NetAmount);
                var discount = order.Sum(x => x.Discount);
                var hold = order.Where(x=>x.Status == false).Count();
                var holdamt = order.Where(x => x.Status == false).Sum(x => x.NetAmount);
                var accepted = order.Where(x => x.Status == true).Count();
                var acceptedamt = order.Where(x => x.Status == true).Sum(x => x.NetAmount);
                //var returs = order.Where(x => x.TransType == "RETURN").Count();
                //var retursamt = order.Where(x => x.TransType == "RETURN").Sum(x => x.NetAmount);
                //retursamt = retursamt * -1;

                ss.tabxablesales = taxsales; ss.nontaxablesales = nontaxsales;
                ss.totalproductsales = taxsales + nontaxsales; ss.itemdiscount = discount;
                ss.netsales = ss.totalproductsales - ss.itemdiscount;
                ss.hold = hold; ss.accepted = accepted; //ss.returns = returs;
                ss.holdamt = holdamt; ss.acceptedamt = acceptedamt; //ss.returnsamt = retursamt;

                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpGet]
        public HttpResponseMessage GetPayementSummary(string flag, DateTime fromdate, DateTime todate)
        {
            try
            {
                SaleSummary ss = new SaleSummary();
                var list = db.Get_PaymentSummary(fromdate, todate).ToList();
                ss.linegraphdata = list.ToArray();

                //var order = (from t in db.SOTransactions
                //             where t.TransactionDate.Month == fdate.Value.Month && t.TransactionDate.Year == fdate.Value.Year
                //             select t).ToList();
                //var taxsales = order.Where(x => x.TaxAmount != 0).Sum(x => x.NetAmount);
                //var nontaxsales = order.Where(x => x.TaxAmount == 0).Sum(x => x.NetAmount);
                //var discount = order.Sum(x => x.Discount);
                //var hold = order.Where(x => x.Status == false).Count();
                //var holdamt = order.Where(x => x.Status == false).Sum(x => x.NetAmount);
                //var accepted = order.Where(x => x.Status == true).Count();
                //var acceptedamt = order.Where(x => x.Status == true).Sum(x => x.NetAmount);


                //ss.tabxablesales = taxsales; ss.nontaxablesales = nontaxsales;
                //ss.totalproductsales = taxsales + nontaxsales; ss.itemdiscount = discount;
                //ss.netsales = ss.totalproductsales - ss.itemdiscount;
                //ss.hold = hold; ss.accepted = accepted; //ss.returns = returs;
                //ss.holdamt = holdamt; ss.acceptedamt = acceptedamt; //ss.returnsamt = retursamt;

                return Request.CreateResponse(HttpStatusCode.OK, ss);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
