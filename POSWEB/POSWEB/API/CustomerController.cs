﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CustomerController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetCustomerList")]
        public HttpResponseMessage GetCustomerList(long Id)
        {
            try
            {
                var list = (from cust in db.Customers
                            join city in db.Cities on cust.CityID equals city.CityID
                            join contry in db.Countries on cust.CountryID equals contry.CountryID
                            join custype in db.CustTypes on cust.CustTypeID equals custype.CustTypeID
                            where cust.Deleted == false
                            select new
                            {
                                CustID = cust.CustID,
                                ShortName = cust.ShortName,
                LongName = cust.LongName,
                District = cust.District,
                CityID = cust.CityID,
                Area = cust.Area,
                Street = cust.Street,
                Address = cust.Address,
                Address1 = cust.Address1,
                CountryID = cust.CountryID,
                Phone = cust.Phone,
                Phone1 = cust.Phone1,
                Fax = cust.Fax,
                Region = cust.Region,
                Mobile = cust.Mobile,
                Email = cust.Email,
                website = cust.website,
                CreditLimit = cust.CreditLimit,
                CreditDays = cust.CreditDays,
                Active = cust.Active,
                PriceList = cust.PriceList,
                Discount = cust.Discount,
                CustTypeID = cust.CustTypeID,
                CityName = city.Name,
                                CountryName = contry.Name,
                                CustType = custype.Description


                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveCustomer")]
        public HttpResponseMessage SaveCustomer(Customer cust)
        {
            try
            {
                if (cust != null)
                {
                    if (cust.CustID != 0 && cust.Deleted_By != 0 && cust.Deleted_By != null)
                    {
                        Customer b = db.Customers.Where(x => x.CustID == cust.CustID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = cust.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else if (cust.CustID != 0 && cust.Deleted_By == 0 && cust.Deleted_By == null)
                    {
                        Customer b = db.Customers.Where(x => x.CustID == cust.CustID).FirstOrDefault();
                        b.ShortName = string.IsNullOrEmpty(cust.ShortName) ? "" : cust.ShortName;
                        b.LongName = cust.LongName;
                        b.District = cust.District;
                        b.CityID = cust.CityID;
                        b.Area = cust.Area;
                        b.Street = cust.Street;
                        b.Address = cust.Address;
                        b.Address1 = cust.Address1;
                        b.CountryID = cust.CountryID;
                        b.Phone = cust.Phone;
                        b.Phone1 = cust.Phone1;
                        b.Fax = cust.Fax;
                        b.Region = cust.Region;
                        b.Mobile = cust.Mobile;
                        b.Email = cust.Email;
                        b.website = cust.website;
                        b.CreditLimit = cust.CreditLimit;
                        b.CreditDays = cust.CreditDays;
                        b.Active = cust.Active;
                        b.PriceList = cust.PriceList;
                        b.Discount = cust.Discount;
                        b.CustTypeID = cust.CustTypeID;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = cust.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                     
                    else
                    {

                        Customer b = new Customer();
                        b.ShortName = string.IsNullOrEmpty(cust.ShortName) ? "" : cust.ShortName;
                        b.LongName = cust.LongName;
                        b.District = cust.District;
                        b.CityID = cust.CityID;
                        b.Area = cust.Area;
                        b.Street = cust.Street;
                        b.Address = cust.Address;
                        b.Address1 = cust.Address1;
                        b.CountryID = cust.CountryID;
                        b.Phone = cust.Phone;
                        b.Phone1 = cust.Phone1;
                        b.Fax = cust.Fax;
                        b.Region = cust.Region;
                        b.Mobile = cust.Mobile;
                        b.Email = cust.Email;
                        b.website = cust.website;
                        b.CreditLimit = cust.CreditLimit;
                        b.CreditDays = cust.CreditDays;
                        b.Active = cust.Active;
                        b.PriceList = cust.PriceList;
                        b.Discount = cust.Discount;
                        b.CustTypeID = cust.CustTypeID;
                        b.Created_By = cust.Created_By;
                        b.Created_Date = DateTime.Now;
                        db.Customers.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, cust);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
