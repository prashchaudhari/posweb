﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CitysController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetCityList")]
        public HttpResponseMessage GetCityList(long Id)
        {
            try
            {
                var list = (from city in db.Cities
                            where city.Deleted == false
                            select new
                            {
                                CityID = city.CityID,
                                CityCode = city.CityCode,
                                Name = city.Name,
                                NameAR = city.NameAR

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveCity")]
        public HttpResponseMessage SaveCity(City city)
        {
            try
            {
                if (city != null)
                {
                    if (city.CityID != 0 && (city.Deleted_By == 0 || city.Deleted_By == null))
                    {
                        City b = db.Cities.Where(x => x.CityID == city.CityID).FirstOrDefault();
                        b.CityCode = string.IsNullOrEmpty(city.CityCode) ? "" : city.CityCode;
                        b.Name = city.Name;
                        b.NameAR = city.NameAR;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = city.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (city.CityID != 0 && city.Deleted_By != 0)
                    {
                        City b = db.Cities.Where(x => x.CityID == city.CityID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = city.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        City b = new City();
                        b.CityCode = string.IsNullOrEmpty(city.CityCode) ? "" : city.CityCode;
                        b.Name = city.Name;
                        b.NameAR = city.NameAR;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = city.Created_by;
                        b.Deleted = false;
                        db.Cities.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, city);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
