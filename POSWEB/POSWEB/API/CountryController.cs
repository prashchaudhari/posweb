﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CountryController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        public HttpResponseMessage GetCountryList(long Id)
        {
            try
            {
                var list = (from country in db.Countries
                            where country.Deleted == false
                            select new
                            {
                                CountryID = country.CountryID,
                                CountryCode = country.CountryCode,
                                Name = country.Name,
                                NameAR = country.NameAR,
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveCountry")]
        public HttpResponseMessage SaveCountry(Country country)
        {
            try
            {
                if (country != null)
                {
                    if (country.CountryID != 0 && (country.Deleted_By == 0 || country.Deleted_By == null))
                    {
                        Country b = db.Countries.Where(x => x.CountryID == country.CountryID).FirstOrDefault();
                        b.CountryCode = string.IsNullOrEmpty(country.CountryCode) ? "" : country.CountryCode;
                        b.Name = country.Name;
                        b.NameAR = country.NameAR;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = country.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (country.CountryID != 0 && country.Deleted_By != 0)
                    {
                        Country b = db.Countries.Where(x => x.CountryID == country.CountryID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = country.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Country b = new Country();
                        b.CountryCode = string.IsNullOrEmpty(country.CountryCode) ? "" : country.CountryCode;
                        b.Name = country.Name;
                        b.NameAR = country.NameAR;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = country.Created_by;
                        b.Deleted = false;
                        db.Countries.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, country);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
