﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class SUserController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetSUserList")]
        public HttpResponseMessage GetSUserList(long Id)
        {
            try
            {
                var list = (from SUser in db.SUsers
                            where SUser.Deleted == false
                            select new
                            {
                                UserID = SUser.UserID,                                
                                UserName = SUser.UserName,
                                RoleID = SUser.RoleID,
                                POSCode = SUser.POSCode,
                                EmployeeID = SUser.EmployeeID                               

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveSUser")]
        public HttpResponseMessage SaveSUser(SUser SUser)
        {
            try
            {
                if (SUser != null)
                {
                    if (SUser.UserID != 0 && (SUser.Deleted_By == 0 || SUser.Deleted_By == null))
                    {
                        SUser b = db.SUsers.Where(x => x.UserID == SUser.UserID).FirstOrDefault();
                        b.UserName = string.IsNullOrEmpty(SUser.UserName) ? "" : SUser.UserName;
                        b.RoleID = SUser.RoleID;
                        b.POSCode = SUser.POSCode;
                        b.EmployeeID = SUser.EmployeeID;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_User = SUser.Modified_User;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (SUser.UserID != 0 && SUser.Deleted_By != 0)
                    {
                        SUser b = db.SUsers.Where(x => x.UserID == SUser.UserID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = SUser.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        SUser b = new SUser();
                        b.UserName = string.IsNullOrEmpty(SUser.UserName) ? "" : SUser.UserName;
                        b.RoleID = SUser.RoleID;
                        b.POSCode = SUser.POSCode;
                        b.EmployeeID = SUser.EmployeeID;
                        b.Created_Date = DateTime.Now;
                        b.Created_User = SUser.Created_User;
                        b.Deleted = false;
                        db.SUsers.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, SUser);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
