﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class ProductTypeController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetProductTypeList")]
        public HttpResponseMessage GetProductTypeList(long Id)
        {
            try
            {
                var list = (from ptype in db.ProductTypes
                            where ptype.Deleted == false
                            select new
                            {
                                ProductTypeID = ptype.ProductTypeID,                               
                                Name = ptype.Name
                                
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveProductType")]
        public HttpResponseMessage SaveProductType(ProductType ptype)
        {
            try
            {
                if (ptype != null)
                {
                    if (ptype.ProductTypeID != 0 && (ptype.Deleted_By == 0 || ptype.Deleted_By == null))
                    {
                        ProductType b = db.ProductTypes.Where(x => x.ProductTypeID == ptype.ProductTypeID).FirstOrDefault();                       
                        b.Name = ptype.Name;                       
                        b.Modified_Date = DateTime.Now;
                        b.Modified_by = ptype.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (ptype.ProductTypeID != 0 && ptype.Deleted_By != 0)
                    {
                        ProductType b = db.ProductTypes.Where(x => x.ProductTypeID == ptype.ProductTypeID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = ptype.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        ProductType b = new ProductType();
                        b.Name = string.IsNullOrEmpty(ptype.Name) ? "" : ptype.Name;                                             
                        b.Created_Date = DateTime.Now;
                        b.Created_By = ptype.Created_By;
                        b.Deleted = false;
                        db.ProductTypes.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, ptype);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
