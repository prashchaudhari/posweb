﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class RoleController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetRoleList")]
        public HttpResponseMessage GetRoleList(long Id)
        {
            try
            {
                var list = (from Role in db.Roles
                            join obj in db.Objects on Role.ObjectID equals obj.ObjectID
                            where Role.Deleted == false
                            select new
                            {
                                RoleID = Role.RoleID,
                                RoleName = Role.RoleName,
                                ObjectID = Role.ObjectID,
                                FullAccess = Role.FullAccess,
                                DisplayAccess = Role.DisplayAccess ,
                                NoAccess = Role.NoAccess ,
                                ObjectType = obj.ObjectType

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveRole")]
        public HttpResponseMessage SaveRole(Role Role)
        {
            try
            {
                if (Role != null)
                {
                    if (Role.RoleID != 0 )
                    {
                        Role b = db.Roles.Where(x => x.RoleID == Role.RoleID).FirstOrDefault();
                        b.RoleName = string.IsNullOrEmpty(Role.RoleName) ? "" : Role.RoleName;
                        b.ObjectID = Role.ObjectID;
                        b.FullAccess = Role.FullAccess;
                        b.DisplayAccess = Role.DisplayAccess;
                        b.NoAccess = Role.NoAccess;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }                    
                    else
                    {
                        // db.Branches.Add(branch);
                        Role b = new Role();
                        b.RoleName = string.IsNullOrEmpty(Role.RoleName) ? "" : Role.RoleName;
                        b.ObjectID = Role.ObjectID;
                        b.FullAccess = Role.FullAccess;
                        b.DisplayAccess = Role.DisplayAccess;
                        b.NoAccess = Role.NoAccess;
                        b.Deleted = false;
                        db.Roles.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, Role);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
