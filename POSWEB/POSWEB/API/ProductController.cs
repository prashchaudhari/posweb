﻿using Newtonsoft.Json.Linq;
using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace POSWEB.API
{
    public class ProductController : ApiController
    {
         DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
        string fileName, proimg;
        [HttpGet]
        [ActionName("GetProductList")]
        public HttpResponseMessage GetProductList(long Id)
        {
            string url = ConfigurationManager.AppSettings["imageUrl"].ToString();
            try
            {
                var list = (from product in db.Products
                            join ptype in db.ProductTypes on product.ProductTypeID equals ptype.ProductTypeID
                            join brand in db.Brands on product.BrandID equals brand.BrandID
                            join cat in db.Categories on product.CategoryID equals cat.CategoryID
                            join tax in db.Taxes on product.TaxID equals tax.TaxID
                            join uom in db.UOMs on product.BaseUnitID equals uom.UOMID
                            join subcat in db.SubCategories on product.SubCategoryID equals subcat.SubCategoryID
                            join grp in db.Groups on product.GroupID equals grp.GroupID
                            where product.Deleted == false
                            select new
                            {
                                ProductID = product.ProductID,
                                ProductTypeID = product.ProductTypeID,
                                Description = product.Description,
                                DescriptionAR = product.DescriptionAR,
                                SubDescription1 = product.SubDescription1,
                                SubDescription2 = product.SubDescription2,
                                Size = product.Size,
                                BrandID = product.BrandID,
                                CategoryID = product.CategoryID,
                                SubCategoryID = product.SubCategoryID,
                                GroupID = product.GroupID,
                                TaxID = product.TaxID,
                                BaseUnitID = product.BaseUnitID,
                                CODE = product.CODE,
                                Model = product.Model,
                                Note = product.Note,
                                Cost = product.Cost,
                                Brand = brand.Description,
                                Category = cat.Description,
                                SubCategory = subcat.Description,
                                Tax = tax.Description,
                                BaseUnit = uom.Description,
                                Group = grp.Description,
                                Name = ptype.Name,
                                proimg = url + product.proimg
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }


        [HttpPost]
        [Route("SaveProduct")]
        public async Task<HttpResponseMessage> Post()
        {
           
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            try
            {
                string filePathName;
                var root = HttpContext.Current.Server.MapPath("~/Uploads/Product");
                //Directory.CreateDirectory(root);
                var provider = new MultipartFormDataStreamProvider(root);
                var result = await Request.Content.ReadAsMultipartAsync(provider);
                var model = result.FormData["model"];
                var obj = JObject.Parse(model);
                long ProductID = Convert.ToInt64(obj["ProductID"]);
                int Deleted_By = Convert.ToInt16(obj["Deleted_By"]);
                string code = (obj["CODE"]).ToString();
                if (ProductID != 0)
                {
                    proimg = obj["proimg"].ToString();
                    proimg = proimg.Split('/').Last();
                }
                foreach (MultipartFileData fileData in provider.FileData)
                {
                    if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                    {
                        return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                    }
                    fileName = fileData.Headers.ContentDisposition.FileName;
                    if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                    {
                        fileName = fileName.Trim('"');
                    }
                    if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                    {
                        fileName = Path.GetFileName(fileName);
                    }
                    if (File.Exists(Path.Combine(root, fileName)))
                    { File.Delete(Path.Combine(root, fileName)); }
                    //if (proimg != "" || proimg != null)
                    //{ File.Delete(Path.Combine(root, proimg)); }
                    File.Move(fileData.LocalFileName, Path.Combine(root, fileName));
                }                
                       
                var file = result.FileData;
                if (file.Count != 0)
                {
                    filePathName = fileName;
                }
                else
                {
                    filePathName = "";
                }

                if (model != null)
                {
                    if (ProductID != 0 && Deleted_By == 0)
                    {
                        Product b = db.Products.Where(x => x.ProductID == ProductID).FirstOrDefault();
                        b.ProductTypeID = Convert.ToInt16(obj["ProductTypeID"]);
                        b.Description = obj["Description"].ToString();
                        b.DescriptionAR = obj["DescriptionAR"].ToString();
                        b.SubDescription1 = obj["SubDescription1"].ToString();
                        b.SubDescription2 = obj["SubDescription2"].ToString();
                        b.Size = obj["Size"].ToString();
                        b.ShowOnHomePage = Convert.ToBoolean(obj["ShowOnHomePage"]) == true ? true : false;
                        b.BrandID = Convert.ToInt16(obj["BrandID"]);
                        b.CategoryID = Convert.ToInt16(obj["CategoryID"]);
                        b.SubCategoryID = Convert.ToInt16(obj["SubCategoryID"]);
                        b.GroupID = Convert.ToInt16(obj["GroupID"]);
                       // b.Model = obj["Model"].ToString();
                        b.TaxID = Convert.ToInt16(obj["TaxID"]);
                        b.BaseUnitID = Convert.ToInt16(obj["BaseUnitID"]);
                        b.Cost = Convert.ToDecimal(obj["Cost"]) != 0 ? Convert.ToDecimal(obj["Cost"]) : 0;
                        b.Note = obj["Note"].ToString();
                        b.CODE = code;
                        if (filePathName != "")
                        {
                            b.proimg = filePathName;
                        }
                        b.Deleted = false;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_by = Convert.ToInt32(obj["Created_by"]);
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Updated");
                    }
                    else
                    {

                        Product b = new Product();
                        b.ProductTypeID = Convert.ToInt16(obj["ProductTypeID"]);
                        b.Description = (obj["Description"]).ToString();
                        b.DescriptionAR = (obj["DescriptionAR"]).ToString();
                        b.SubDescription1 = (obj["SubDescription1"]).ToString();
                        b.SubDescription2 = (obj["SubDescription2"]).ToString();
                        b.Size = (obj["Size"]).ToString();
                        b.ShowOnHomePage = Convert.ToBoolean(obj["ShowOnHomePage"]);
                        b.BrandID = Convert.ToInt16(obj["BrandID"]);
                        b.CategoryID = Convert.ToInt16(obj["CategoryID"]);
                        b.SubCategoryID = Convert.ToInt16(obj["SubCategoryID"]);
                        b.GroupID = Convert.ToInt16(obj["GroupID"]);
                        b.Model = (obj["Model"]).ToString();
                        b.TaxID = Convert.ToInt16(obj["TaxID"]);
                        b.BaseUnitID = Convert.ToInt16(obj["BaseUnitID"]);
                        b.Cost = Convert.ToDecimal(obj["Cost"]) != 0 ? Convert.ToDecimal(obj["Cost"]) : 0;
                        b.Note = (obj["Note"]).ToString();
                        b.CODE = code;
                        b.proimg = filePathName;
                        b.Created_By = Convert.ToInt16(obj["Created_by"]);
                        b.Deleted = false;
                        db.Products.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }

                    
                }
                else
                {
                    ////TODO: Do something with the JSON data.  
                    ////get the posted files  
                    //foreach (var file in result.FileData)
                    //{
                    //    //TODO: Do something with uploaded file. 

                    //}

                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

       
        [HttpPost]
        [ActionName("UpdateProduct")]
        public HttpResponseMessage UpdateProduct(Product product)
        {
            try
            {
                if (product != null)
                {
                    if (product.ProductID != 0 && product.Deleted_By == 0)
                    {
                        Product b = db.Products.Where(x => x.ProductID == product.ProductID).FirstOrDefault();
                        b.ProductTypeID = product.ProductTypeID;
                        b.Description = product.Description;
                        b.DescriptionAR = product.DescriptionAR;
                        b.SubDescription1 = product.SubDescription1;
                        b.SubDescription2 = product.SubDescription2;
                        b.Size = product.Size;
                        b.ShowOnHomePage = product.ShowOnHomePage;
                        b.BrandID = product.BrandID;
                        b.CategoryID = product.CategoryID;
                        b.SubCategoryID = product.SubCategoryID;
                        b.GroupID = product.GroupID;
                        b.Model = product.Model;
                        b.TaxID = product.TaxID;
                        b.BaseUnitID = product.BaseUnitID;
                        b.Cost = product.Cost;
                        b.Note = product.Note;
                       // b.CODE = product.CODE;
                        b.Deleted = false;
                        //b.proimg = product.proimg;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_by = product.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else 
                    {
                        Product b = db.Products.Where(x => x.ProductID == product.ProductID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = product.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }                   
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        private static string GetRoot(string root)
        {
            return root;
        }


    }
}
