﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class Conversi0nController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

      
        public HttpResponseMessage GetConversi0nList(long Id)
        {
            try
            {
                var list = (from conv in db.Conversi0n
                            join prod in db.Products on conv.ProductID equals prod.ProductID
                            join uom in db.UOMs on conv.UOMID equals uom.UOMID
                            //join custype in db.CustTypes on cust.CustTypeID equals custype.CustTypeID
                            where conv.Deleted == false
                            select new
                            {
                                AutoID = conv.AutoID,
                                EAN = conv.EAN,
                                ProductID =conv.ProductID,
                                UOMID =conv.UOMID,
                                BaseUnit = conv.BaseUnit,
                                Conv = conv.Conv,
                                Price = conv.Price,
                                PriceA = conv.PriceA,
                                PriceB = conv.PriceB,
                                PriceC = conv.PriceC,
                                DiscountPercent = conv.DiscountPercent,
                                DiscountPrice = conv.DiscountPrice,
                                LeastPrice = conv.LeastPrice,
                                SaleEnd = conv.SaleEnd,
                                SaleStart = conv.SaleStart,
                                ProductDescription = prod.Description,
                                Uom = uom.Description


                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        public HttpResponseMessage SaveConversi0n(Conversi0n cust)
        {
            try
            {
                if (cust != null)
                {
                    if (cust.AutoID != 0 && cust.Deleted_By != 0 && cust.Deleted_By != null)
                    {
                        Conversi0n b = db.Conversi0n.Where(x => x.AutoID == cust.AutoID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = cust.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else if (cust.AutoID != 0 && (cust.Deleted_By == 0 || cust.Deleted_By == null))
                    {
                        Conversi0n b = db.Conversi0n.Where(x => x.AutoID == cust.AutoID).FirstOrDefault();
                        b.EAN = string.IsNullOrEmpty(cust.EAN) ? "" : cust.EAN;
                        b.ProductID = cust.ProductID;
                        b.BaseUnit = cust.BaseUnit;
                        b.UOMID = cust.UOMID;
                        b.Conv = cust.Conv;
                        b.Price = cust.Price;
                        b.PriceA = cust.PriceA;
                        b.PriceB = cust.PriceB;
                        b.PriceC = cust.PriceC;
                        b.LeastPrice = cust.LeastPrice;
                        b.DiscountPrice = cust.DiscountPrice;
                        b.DiscountPercent = cust.DiscountPercent;
                        b.SaleStart = cust.SaleStart;
                        b.SaleEnd = cust.SaleEnd;                       
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = cust.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }

                    else
                    {

                        Conversi0n b = new Conversi0n();
                        b.EAN = string.IsNullOrEmpty(cust.EAN) ? "" : cust.EAN;
                        b.ProductID = cust.ProductID;
                        b.BaseUnit = cust.BaseUnit;
                        b.UOMID = cust.UOMID;
                        b.Conv = cust.Conv;
                        b.Price = cust.Price;
                        b.PriceA = cust.PriceA;
                        b.PriceB = cust.PriceB;
                        b.PriceC = cust.PriceC;
                        b.LeastPrice = cust.LeastPrice;
                        b.DiscountPrice = cust.DiscountPrice;
                        b.DiscountPercent = cust.DiscountPercent;
                        b.SaleStart = cust.SaleStart;
                        b.SaleEnd = cust.SaleEnd;
                        b.Created_by = cust.Created_by;
                        b.Created_Date = DateTime.Now;
                        b.Deleted = false;
                        db.Conversi0n.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, cust);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
