﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class TaxesController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetTaxesList")]
        public HttpResponseMessage GetTaxesList(long Id)
        {
            try
            {
                var list = (from tax in db.Taxes                           
                            where tax.Deleted_By == 0 || tax.Deleted_By==null
                            select new
                            {
                                TaxID = tax.TaxID,
                                TaxFixed = tax.TaxFixed,
                                TaxPercentage = tax.TaxPercentage,
                                Description = tax.Description


                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
