﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class GoodReceiveController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();
        public HttpResponseMessage GetGoodReceiveList(long Id)
        {
            try
            {
                var list = (from gr in db.GoodRecieves
                            join branch in db.Branches on gr.BranchID equals branch.BranchID
                            join slocation in db.SLocations on gr.SLocationID equals slocation.SLocationID
                            join vend in db.Vendors on gr.VendorID equals vend.VendorID
                            where gr.Deleted == false
                            select new
                            {
                                AutoID = gr.AutoID,
                                POReleaseDate = gr.POReleaseDate,
                                BranchID = gr.BranchID,
                                SLocationID = gr.SLocationID,
                                SLocationName = slocation.Name,
                                TransType = gr.TransType,
                                PONumber = gr.PONumber,
                                VendorID = gr.VendorID,
                                VendorShortName = vend.ShortName,
                                BuyerID = gr.BuyerID,
                                PODate = gr.PODate,
                                ReleaseStatus = gr.ReleaseStatus,
                                FixDiscount = gr.FixDiscount,
                                SubTotal = gr.SubTotal,
                                VAT = gr.VAT,
                                NetAmount = gr.NetAmount,                                
                                BranchName = branch.BranchName
                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        public HttpResponseMessage SaveGoodReceive(GoodRecieve subcat)
        {
            try
            {
                if (subcat != null)
                {
                    if (subcat.AutoID != 0 && (subcat.Deleted_By == 0 || subcat.Deleted_By == null))
                    {
                        GoodRecieve b = db.GoodRecieves.Where(x => x.AutoID == subcat.AutoID).FirstOrDefault();
                        b.TransType = subcat.TransType;
                        b.BranchID = subcat.BranchID;
                        b.PONumber = subcat.PONumber;
                        b.VendorID = subcat.VendorID;
                        b.SLocationID = subcat.SLocationID;
                        b.POReleaseDate = subcat.POReleaseDate;
                        b.BuyerID = subcat.BuyerID;                       
                        b.PODate = subcat.PODate;
                        b.ReleaseStatus = subcat.ReleaseStatus;
                        b.FixDiscount = subcat.FixDiscount;
                        b.SubTotal = subcat.SubTotal;
                        b.VAT = subcat.VAT;                       
                        b.NetAmount = subcat.NetAmount;                       
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = subcat.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (subcat.AutoID != 0 && subcat.Deleted_By != 0)
                    {
                        GoodRecieve b = db.GoodRecieves.Where(x => x.AutoID == subcat.AutoID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = subcat.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        GoodRecieve b = new GoodRecieve();
                        b.TransType = subcat.TransType;
                        b.BranchID = subcat.BranchID;
                        b.PONumber = subcat.PONumber;
                        b.VendorID = subcat.VendorID;
                        b.SLocationID = subcat.SLocationID;
                        b.POReleaseDate = subcat.POReleaseDate;
                        b.BuyerID = subcat.BuyerID;
                        b.PODate = subcat.PODate;
                        b.ReleaseStatus = subcat.ReleaseStatus;
                        b.FixDiscount = subcat.FixDiscount;
                        b.SubTotal = subcat.SubTotal;
                        b.VAT = subcat.VAT;
                        b.NetAmount = subcat.NetAmount;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = subcat.Created_by;
                        b.Deleted = false;
                        db.GoodRecieves.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, subcat);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
