﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using POSWEB.Models;

namespace POSWEB.API
{
    public class VendorController : ApiController
    {
        
            DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

            [HttpGet]
            [ActionName("GetVendorList")]
            public HttpResponseMessage GetVendorList(long Id)
            {
                try
                {
                    var list = (from v in db.Vendors
                                where v.Deleted == false
                                select new
                                {
                                    v.VendorID,
                                    VendorTypeID = v.VendorType,
                                    VendorTypeName = v.VendorType1.Description,
                                    v.ShortName,
                                    v.LongName,
                                    v.Address,
                                    v.Address1,
                                    v.Area,
                                    v.Street,
                                    v.District,
                                    v.CityID,
                                    CityName = v.City.Name,
                                    v.Region,
                                    v.CountryID,
                                    CountryName = v.Country.Name,
                                    v.Phone,
                                    v.Phone1,
                                    v.Fax,
                                    v.Mobile,
                                    v.Email,
                                    v.website,
                                    v.PaymentType,
                                    v.CreditDays,
                                    v.Active,
                                    v.PriceList

                                }).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
                }
            }

            [HttpPost]
            [ActionName("SaveVendor")]
            public HttpResponseMessage SaveVendor(Vendor v)
            {
                try
                {
                    if (v != null)
                    {
                        if (v.VendorID != 0 && (v.Deleted_By == 0 || v.Deleted_By == null))
                        {
                            Vendor b = db.Vendors.Where(x => x.VendorID == v.VendorID).FirstOrDefault();
                            b.VendorID = v.VendorID;
                            b.VendorType = v.VendorType;
                            b.ShortName = v.ShortName;
                            b.LongName = v.LongName;
                            b.Address = v.Address;
                            b.Address1 = v.Address1;
                            b.Area = v.Area;
                            b.Street = v.Street;
                            b.District = v.District;
                            b.CityID = v.CityID;
                            b.Region = v.Region;
                            b.CountryID = v.CountryID;
                            b.Phone = v.Phone;
                            b.Phone1 = v.Phone1;
                            b.Fax = v.Fax;
                            b.Mobile = v.Mobile;
                            b.Email = v.Email;
                            b.website = v.website;
                            b.PaymentType = v.PaymentType;
                            b.CreditDays = v.CreditDays;
                            b.Active = v.Active;
                            b.PriceList = v.PriceList;
                            b.Modified_Date = DateTime.Now;
                            b.Modified_By = v.Created_By;
                            db.SaveChanges();

                            return Request.CreateResponse(HttpStatusCode.OK, "success");
                        }
                        else if (v.VendorID != 0 && v.Deleted_By != 0)
                        {
                            Vendor b = db.Vendors.Where(x => x.VendorID == v.VendorID).FirstOrDefault();

                            b.Deleted = true;
                            b.Deleted_By = v.Deleted_By;
                            b.Deleted_Date = DateTime.Now;
                            db.SaveChanges();

                            return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                        }
                        else
                        {
                            // db.Branches.Add(branch);
                            Vendor b = new Vendor();
                            b.ShortName = string.IsNullOrEmpty(v.ShortName) ? "" : v.ShortName;
                            b.VendorID = v.VendorID;
                            b.VendorType = v.VendorType;
                            b.LongName = v.LongName;
                            b.Address = v.Address;
                            b.Address1 = v.Address1;
                            b.Area = v.Area;
                            b.Street = v.Street;
                            b.District = v.District;
                            b.CityID = v.CityID;
                            b.Region = v.Region;
                            b.CountryID = v.CountryID;
                            b.Phone = v.Phone;
                            b.Phone1 = v.Phone1;
                            b.Fax = v.Fax;
                            b.Mobile = v.Mobile;
                            b.Email = v.Email;
                            b.website = v.website;
                            b.PaymentType = v.PaymentType;
                            b.CreditDays = v.CreditDays;
                            b.Active = v.Active;
                            b.PriceList = v.PriceList;
                            b.Created_Date = DateTime.Now;
                            b.Created_By = v.Created_By;
                            b.Deleted = false;
                            db.Vendors.Add(b);
                            db.SaveChanges();


                            return Request.CreateResponse(HttpStatusCode.OK, v);
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                    }
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
                }
            }
        }
    
}
