﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class SubCategoryController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetSubCategoryList")]
        public HttpResponseMessage GetSubCategoryList(long Id)
        {
            try
            {
                var list = (from subcat in db.SubCategories
                            join maincat in db.Categories on subcat.CategoryID equals maincat.CategoryID
                            where subcat.Deleted == false
                            select new
                            {
                                SubCategoryID = subcat.SubCategoryID,
                                CategoryID = subcat.CategoryID,
                                Description = subcat.Description,
                                Category = maincat.Description
                                

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpGet]
        [ActionName("GetSubCategory")]
        public HttpResponseMessage GetSubCategory(long Cat,string flag)
        {
            try
            {
                if (Cat != 0)
                {
                    var list = (from subcat in db.SubCategories
                                join maincat in db.Categories on subcat.CategoryID equals maincat.CategoryID
                                where subcat.Deleted == false && subcat.CategoryID == Cat
                                select new
                                {
                                    SubCategoryID = subcat.SubCategoryID,
                                    CategoryID = subcat.CategoryID,
                                    Description = subcat.Description,
                                    Category = maincat.Description


                                }).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
                else
                {
                    var list = (from subcat in db.SubCategories
                                join maincat in db.Categories on subcat.CategoryID equals maincat.CategoryID
                                where subcat.Deleted == false
                                select new
                                {
                                    SubCategoryID = subcat.SubCategoryID,
                                    CategoryID = subcat.CategoryID,
                                    Description = subcat.Description,
                                    Category = maincat.Description


                                }).ToList();
                    return Request.CreateResponse(HttpStatusCode.OK, list);
                }
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveSubCategory")]
        public HttpResponseMessage SaveSubCategory(SubCategory subcat)
        {
            try
            {
                if (subcat != null)
                {
                    if (subcat.SubCategoryID != 0 && (subcat.Deleted_By == 0 || subcat.Deleted_By == null))
                    {
                        SubCategory b = db.SubCategories.Where(x => x.SubCategoryID == subcat.SubCategoryID).FirstOrDefault();                       
                        b.Description = subcat.Description;
                        b.CategoryID = subcat.CategoryID;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = subcat.Modified_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (subcat.SubCategoryID != 0 && subcat.Deleted_By != 0)
                    {
                        SubCategory b = db.SubCategories.Where(x => x.SubCategoryID == subcat.SubCategoryID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = subcat.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        SubCategory b = new SubCategory();                       
                        b.Description = subcat.Description;
                        b.CategoryID = subcat.CategoryID;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = subcat.Created_by;
                        b.Deleted = false;
                        db.SubCategories.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, subcat);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
