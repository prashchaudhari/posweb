﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class CurrencyController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        [HttpGet]
        [ActionName("GetCurrencyList")]
        public HttpResponseMessage GetCurrencyList(long Id)
        {
            try
            {
                var list = (from currn in db.Currencies
                            where currn.Deleted == false
                            select new
                            {
                                CurrencyID = currn.CurrencyID,
                                ISO = currn.ISO,
                                Description = currn.Description

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        [HttpPost]
        [ActionName("SaveCurrency")]
        public HttpResponseMessage SaveCurrency(Currency currn)
        {
            try
            {
                if (currn != null)
                {
                    if (currn.CurrencyID != 0 && (currn.Deleted_By == 0 || currn.Deleted_By == null))
                    {
                        Currency b = db.Currencies.Where(x => x.CurrencyID == currn.CurrencyID).FirstOrDefault();
                        b.Description = string.IsNullOrEmpty(currn.Description) ? "" : currn.Description;
                        b.ISO = currn.ISO;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = currn.Created_by;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (currn.CurrencyID != 0 && currn.Deleted_By != 0)
                    {
                        Currency b = db.Currencies.Where(x => x.CurrencyID == currn.CurrencyID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = currn.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Currency b = new Currency();
                        b.Description = string.IsNullOrEmpty(currn.Description) ? "" : currn.Description;
                        b.ISO = currn.ISO;
                        b.Created_Date = DateTime.Now;
                        b.Created_by = currn.Created_by;
                        b.Deleted = false;
                        db.Currencies.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, currn);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
