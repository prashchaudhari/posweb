﻿using POSWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace POSWEB.API
{
    public class BrandController : ApiController
    {
        DB_A269C5_ServerPOSTestEntities db = new DB_A269C5_ServerPOSTestEntities();

        
        public HttpResponseMessage GetBrandList(long Id)
        {
            try
            {
                var list = (from brand in db.Brands
                            where brand.Deleted == false
                            select new
                            {
                                BrandID = brand.BrandID,
                                Description = brand.Description,
                                ColorCode = brand.ColorCode   

                            }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }

        
        public HttpResponseMessage SaveBrand(Brand brand)
        {
            try
            {
                if (brand != null)
                {
                    if (brand.BrandID != 0 && (brand.Deleted_By == 0 || brand.Deleted_By == null))
                    {
                        Brand b = db.Brands.Where(x => x.BrandID == brand.BrandID).FirstOrDefault();
                        b.Description = string.IsNullOrEmpty(brand.Description) ? "" : brand.Description;
                        b.ColorCode = brand.ColorCode;
                        b.Modified_Date = DateTime.Now;
                        b.Modified_By = brand.Created_By;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "success");
                    }
                    else if (brand.BrandID != 0 && brand.Deleted_By != 0)
                    {
                        Brand b = db.Brands.Where(x => x.BrandID == brand.BrandID).FirstOrDefault();

                        b.Deleted = true;
                        b.Deleted_By = brand.Deleted_By;
                        b.Deleted_Date = DateTime.Now;
                        db.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                    }
                    else
                    {
                        // db.Branches.Add(branch);
                        Brand b = new Brand();
                        b.Description = string.IsNullOrEmpty(brand.Description) ? "" : brand.Description;
                        b.ColorCode = brand.ColorCode;
                        b.Created_Date = DateTime.Now;
                        b.Created_By = brand.Created_By;
                        b.Deleted = false;
                        db.Brands.Add(b);
                        db.SaveChanges();


                        return Request.CreateResponse(HttpStatusCode.OK, brand);
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Parameter is null");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new { Error = ex.InnerException.Message });
            }
        }
    }
}
